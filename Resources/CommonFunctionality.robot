*** Settings  *** 
# นำเข้า Libary ที่จะใช้งาน
Library  Selenium2Library  

*** Variables ***
# url เป็น ลิงก์ของเว็บไซต์
&{url}    dev=https://teleconsult-dev.telehealththai.com    stag=https://teleconsult-stag.telehealththai.com    prod=https://teleconsult.telehealththai.com    prod2=https://account.telehealththai.com/main/login
# Browser เป็น browser ที่จะใช้ทดสอบ
&{Browser}    chrome=headlesschrome    firefox=firefox

*** Keywords *** 
Start Test Case   
    Comment   เปิด Browser ใหม่
    Open Browser    ${url.${env}}    ${Browser.chrome}    options=add_argument("window-size=1920,1080")
    Comment   ขยาย หน้าจอให้เต็มจอ
    Maximize Browser Window 

Closed Browser 
    Comment   ปิด Browser
    Close Browser

Set Time Out
    Comment   ตั้งค่าให้ robot รอเพื่อตรวจสอบเงื่อนไขจนกว่าจะ Timeout
    Set Selenium Timeout    60s


