*** Settings  ***
# นำเข้า Libary ที่จะใช้งาน
Library  Selenium2Library

*** Keywords ***
Input valid username and password	
    [Arguments]    ${sleeptime}    ${username}    ${password}

    Comment    ระบุ username
    Wait Until Element Is Visible    xpath=//*[@id="loginform"]/div[3]/div[1]/input    10s
    Input Text    xpath=//*[@id="loginform"]/div[3]/div[1]/input    ${username}

    Comment    ระบุ password
    Wait Until Element Is Visible    xpath=//*[@id="loginform"]/div[3]/div[2]/input    10s
    Input Text    xpath=//*[@id="loginform"]/div[3]/div[2]/input    ${password}

    Comment   กดปุ่มล็อคอิน
    Wait Until Element Is Visible    xpath=//*[@id="loginform"]/div[5]/div/button    10s
    Click Button    xpath=//*[@id="loginform"]/div[5]/div/button   

Verify landing page
    [Arguments]    ${sleeptime}    ${officer_name}	

    Comment    ตรวจสอบหน้าจอ landing page
    Comment    ตรวจสอบ ชื่อนามสกุลของผู้เข้ารับบริการ
    Wait Until Element Is Visible   xpath=//body[1]/section[1]/div[1]/div[1]/div[1]/form[1]/div[3]/a[1]    10s
    Page Should Contain    ${officer_name} 

    Comment    ตรวจสอบปุ่มในหน้าจอ landing page
    ${btn1} =    Get Text    xpath=//body[1]/section[1]/div[1]/div[1]/div[1]/form[1]/div[3]/a[1]  
    Should Be Equal    ระบบคัดกรองโรค (Telehealth)    ${btn1}
    ${btn2} =    Get Text    xpath=//body[1]/section[1]/div[1]/div[1]/div[1]/form[1]/div[4]/a[1]  
    Should Be Equal    ระบบติดตามอาการผู้ป่วย (H.I.M.)    ${btn2}
    ${btn3} =    Get Text    xpath=//body[1]/section[1]/div[1]/div[1]/div[1]/form[1]/div[6]/a[1]
    Should Be Equal    ออกจากระบบ    ${btn3}

Choose telehealth menu
    [Arguments]    ${sleeptime}

    Comment    เลือกปุ่ม ระบบคัดกรองโรค (Telehealth)
    Wait Until Element Is Visible   xpath=//body[1]/section[1]/div[1]/div[1]/div[1]/form[1]/div[3]/a[1]    10s
    Click Link    xpath=//body[1]/section[1]/div[1]/div[1]/div[1]/form[1]/div[3]/a[1]

Verify doctor login profile
    [Arguments]    ${sleeptime}    ${doctor_name}	

    Comment    ตรวจสอบข้อมูลผู้ใช้งาน
    Comment    ตรวจสอบชื่อ header หน้าจอ
    Wait Until Element Is Visible   xpath=//h1[text()='Telehealth']    60s

    Comment    ตรวจสอบชื่อนามสกุล ที่เมนู ขวามือบน
    Wait Until Element Is Visible   xpath=//body[1]/div[2]/div[1]/div[1]/nav[1]/div[1]/ul[1]/li[1]/div[1]/button[1]/div[1]/div[1]/span[1]    60s
    Element Should Contain    xpath=//body[1]/div[2]/div[1]/div[1]/nav[1]/div[1]/ul[1]/li[1]/div[1]/button[1]/div[1]/div[1]/span[1]    ${doctor_name}




