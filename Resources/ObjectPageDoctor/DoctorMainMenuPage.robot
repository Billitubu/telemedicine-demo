*** Settings  ***
# นำเข้า Libary ที่จะใช้งาน
Library    Selenium2Library
Library    String
Library    DateTime

*** Keywords ***
Verify all main menu
    [Arguments]    ${sleeptime}	

    Comment    ตรวจสอบ title name ของ Browser
    ${WindowTitle}=    Get Title
    Should Be Equal    TeleHealth    ${WindowTitle}

    Comment    ตรวจสอบชื่อเมนูหลัก
    Wait Until Element Is Visible    xpath=//body/div[@id='main-wrapper']/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/a[1]/div[1]/div[1]/p    10s
    ${Menu1_name} =    Get Text    xpath=//body/div[@id='main-wrapper']/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/a[1]/div[1]/div[1]/p
    Should Be Equal    รายการรอให้คำแนะนำ    ${Menu1_name}
    ${Menu2_name} =    Get Text    xpath=//body/div[@id='main-wrapper']/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/a[1]/div[1]/div[1]/p
    Should Be Equal    จัดการเทมเพลต    ${Menu2_name}
    ${Menu3_name} =    Get Text    xpath=//body/div[@id='main-wrapper']/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[3]/a[1]/div[1]/div[1]/p
    Should Be Equal    ประวัติการให้บริการ    ${Menu3_name}
    ${Menu4_name} =    Get Text    xpath=//body/div[@id='main-wrapper']/div[1]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/a[1]/div[1]/div[1]/p
    Should Be Equal    ระบบประชุมทางไกล    ${Menu4_name}
    ${Menu5_name} =    Get Text    xpath=//body/div[@id='main-wrapper']/div[1]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[2]/a[1]/div[1]/div[1]/p
    Should Be Equal    นัดหมายขอคำปรึกษา    ${Menu5_name}
    ${Menu6_name} =    Get Text    xpath=//body/div[@id='main-wrapper']/div[1]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[3]/a[1]/div[1]/div[1]/p
    Should Be Equal    ระบบปรึกษาปัญหาสุขภาพ    ${Menu6_name}