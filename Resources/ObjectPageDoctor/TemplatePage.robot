*** Settings  ***
# นำเข้า Libary ที่จะใช้งาน
Library    Selenium2Library
Library    String
Library    DateTime

*** Keywords ***
Verify list template screen
    [Arguments]    ${sleeptime}

    Comment    กดปุ่ม "จัดการเทมเพลต"
	Wait Until Element Is Visible    xpath=//body/div[@id='main-wrapper']/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/a[1]    10s
    Click Element    xpath=//body/div[@id='main-wrapper']/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/a[1]

    Comment    ตรวจสอบข้อความ Breadcrumb ที่แสดงในหน้าจอ "รายการเทมเพลต"
    Wait Until Element Is Visible    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[1]/ol[1]/li[2]    10s
    ${Listtemplate_pathlink} =    Get Text    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[1]/ol[1]/li[2]
    Should Contain    ${Listtemplate_pathlink}    รายการเทมเพลต

    Comment    ตรวจสอบข้อความ ชื่อหน้าจอ ที่แสดงในหน้าจอ "รายการเทมเพลต"
    Wait Until Element Is Visible    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/div[1]/div[1]/h3[1]/b    10s
    ${Listtemplate_screenname} =    Get Text    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/div[1]/div[1]/h3[1]/b
    Should Contain    ${Listtemplate_screenname}    รายการเทมเพลต

    Comment    ตรวจสอบ input ที่หน้าจอ "รายการเทมเพลต"
    Comment    ตรวจสอบ Textbox ค้นหา
    Element Should Be Visible    xpath=//input[@id='txt_search']
    Comment    ตรวจสอบ ปุ่ม เพิ่มเทมเพลต
    ${Listtemplate_btnadd} =    Get Text    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/form[1]/div[1]/div[2]/button[1]
    Should Be Equal    เพิ่มเทมเพลต    ${Listtemplate_btnadd}

    Comment    ตรวจสอบชื่อคอลัมน์ที่ตารางในหน้าจอ "รายการเทมเพลต"
    Wait Until Element Is Visible    xpath=//thead/tr[1]/th[1]    10s
    ${Listtemplate_tblcolumn1} =    Get Text    xpath=//thead/tr[1]/th[1]
    Should Be Equal    ลำดับ    ${Listtemplate_tblcolumn1}
    ${Listtemplate_tblcolumn2} =    Get Text    xpath=//thead/tr[1]/th[2]
    Should Be Equal    หัวข้อ    ${Listtemplate_tblcolumn2}
    ${Listtemplate_tblcolumn3} =    Get Text    xpath=//thead/tr[1]/th[3]
    Should Be Equal    รายละเอียด    ${Listtemplate_tblcolumn3}
    ${Listtemplate_tblcolumn4} =    Get Text    xpath=//thead/tr[1]/th[4]
    Should Be Equal    Action    ${Listtemplate_tblcolumn4}


Add template record
    [Arguments]    ${sleeptime}    ${topicname}    ${detail}

    Comment    กดปุ่ม เพิ่มเทมเพลต
    Wait Until Element Is Visible    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/form[1]/div[1]/div[2]/button[1]    10s
    Click Button    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/form[1]/div[1]/div[2]/button[1]

    Comment    ตรวจสอบ Popup
    Wait Until Element Is Visible    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/h5[1]    10s
    ${popupaddtemplate_header} =    Get Text    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/h5[1]
    Should Be Equal    เพิ่มเทมเพลต    ${popupaddtemplate_header}
    ${popupaddtemplate_topic} =    Get Text    xpath=//body[1]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[2]/form[1]/div[1]/div[1]/h4[1]
    Should Be Equal    หัวข้อ    ${popupaddtemplate_topic}
    ${popupaddtemplate_detail} =    Get Text    xpath=//body[1]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[2]/form[1]/div[1]/div[3]/h4[1]
    Should Be Equal    รายละเอียด    ${popupaddtemplate_detail}
    ${popupaddtemplate_btncancel} =    Get Text    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[3]/div[1]/div[1]/div[3]/button[1]
    Should Be Equal    ยกเลิก    ${popupaddtemplate_btncancel}
    ${popupaddtemplate_btnsubmit} =    Get Text    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[3]/div[1]/div[1]/div[3]/button[2]
    Should Be Equal    ตกลง    ${popupaddtemplate_btnsubmit}
       
    Comment    เพิ่มข้อมูล template ใน Popup
    Comment    ระบุ ชื่อ template
    Wait Until Element Is Visible     xpath=//input[@id='txt_subject_i']    10s
    Input Text    xpath=//input[@id='txt_subject_i']    ${topicname}
    Comment    ระบุ รายละเอียด template
    Wait Until Element Is Visible     xpath=//textarea[@id='txt_detail_i']    10s
    Input Text    xpath=//textarea[@id='txt_detail_i']    ${detail}

    Comment    กดปุม บันทึก
    Wait Until Element Is Visible     xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[3]/div[1]/div[1]/div[3]/button[2]     10s
    Click Button    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[3]/div[1]/div[1]/div[3]/button[2] 
    
    Comment    ตรวจสอบ หลังบันทึก
    Comment    ตรวจสอบ แสดงข้อความ บันทึกสำเร็จ
    Wait Until Element Is Visible    xpath=//div[@id='swal2-content']    10s
    ${popupaftersubmit_text} =    Get Text    xpath=//div[@id='swal2-content']
    Should Be Equal    บันทึกสำเร็จ    ${popupaftersubmit_text}
    Comment    ตรวจสอบ ชื่อปุ่ม ok
    ${popupaftersubmit_btn} =    Get Text    xpath=//button[contains(text(),'OK')]
    Should Be Equal    OK    ${popupaftersubmit_btn}

    Comment     กดปุ่ม ok เพื่อปิด popup
    Wait Until Element Is Visible    xpath=//button[contains(text(),'OK')]    10s
    Click Button    xpath=//button[contains(text(),'OK')]

Verify search template record by topic name
    [Arguments]    ${sleeptime}    ${topicname}

    Comment    ระบุเงื่อนไขในการค้นหา ใน textbox
    Wait Until Element Is Visible    xpath=//input[@id='txt_search']    10s
    Input Text    xpath=//input[@id='txt_search']    ${topicname}

    Comment    ค้นหา เรียงผลลัพธ์ ให้รายการที่เพิ่มล่าสุดอยู่ด้านบน
    Wait Until Element Is Visible    xpath=//thead/tr[1]/th[2]    10s
    Click Element    xpath=//thead/tr[1]/th[2]
    Click Element    xpath=//thead/tr[1]/th[2]

    Comment    ตรวจสอบผลลัพธ์รายการที่ได้จากการค้นหาข้อมูล
    Wait Until Element Is Visible    xpath=//tbody/tr[1]/td[2]     10s
    ${searchresult_topic} =    Get Text    xpath=//tbody/tr[1]/td[2]
    Should Contain    ${searchresult_topic}    ${topicname}

Delete template record
    [Arguments]    ${sleeptime}

    Comment    กดปุ่ม ลบ รายการ
    Wait Until Element Is Visible    xpath=//tbody/tr[1]/td[4]/button[2]    10s
    Click Button    xpath=//tbody/tr[1]/td[4]/button[2]

    Comment    ตรวจสอบ Popup
    Wait Until Element Is Visible    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[5]/div[1]/div[1]/div[1]/h5[1]    10s
    ${popupdeletetemplate_header} =    Get Text    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[5]/div[1]/div[1]/div[1]/h5[1]
    Should Be Equal    ลบเทมเพลต    ${popupdeletetemplate_header}
    ${popupdeletetemplate_text} =    Get Text    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[5]/div[1]/div[1]/div[2]/h3[1]
    Should Be Equal    คุณต้องการลบเทมเพลตนี้ใช่หรือไม่    ${popupdeletetemplate_text}
    ${popupdeletetemplate_btncancel} =    Get Text    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[5]/div[1]/div[1]/div[3]/button[1]
    Should Be Equal    ยกเลิก    ${popupdeletetemplate_btncancel}
    ${popupdeletetemplate_btnconfirm} =    Get Text    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[5]/div[1]/div[1]/div[3]/button[2]
    Should Be Equal    ยืนยัน    ${popupdeletetemplate_btnconfirm}

    Comment    ยืนยัน ลบ รายการ
    Wait Until Element Is Visible    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[5]/div[1]/div[1]/div[3]/button[2]    10s
    Click Button    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[5]/div[1]/div[1]/div[3]/button[2]
    
    Comment    ตรวจสอบ Popup หลังยืนยันลบ
    Wait Until Element Is Visible    xpath=//div[@id='swal2-content']    10s
    ${popupdeletetemplate_confirm} =    Get Text    xpath=//div[@id='swal2-content']
    Should Be Equal    ลบสำเร็จ    ${popupdeletetemplate_confirm}
    ${popupdeletetemplate_btnconfirm} =    Get Text    xpath=//button[contains(text(),'OK')]
    Should Be Equal    OK    ${popupdeletetemplate_btnconfirm}

    Comment     กดปุ่ม ok เพื่อปิด popup
    Click Button    xpath=//button[contains(text(),'OK')]





    
