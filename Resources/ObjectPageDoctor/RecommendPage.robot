*** Settings  ***
# นำเข้า Libary ที่จะใช้งาน
Library    Selenium2Library
Library    String
Library    DateTime
Resource   ../../Resources/ObjectPageDoctor/DoctorLoginPage.robot
Resource   ../../Resources/ObjectPageDoctor/DoctorMainMenuPage.robot
Resource   ../../Resources/ObjectPageDoctor/TemplatePage.robot
Resource   ../../Resources/ObjectPageDoctor/DoctorListHistoryPage.robot
Resource   ../../Resources/ObjectPageOfficer/OfficerLoginPage.robot
Resource   ../../Resources/ObjectPageOfficer/ListDailyPage.robot
Resource   ../../Resources/ObjectPageOfficer/OfficerListHistoryPage.robot

*** Keywords ***
Verify list recommend screen
    [Arguments]    ${sleeptime}

    Comment    กดปุ่ม "รายการรอให้คำแนะนำ"
	Wait Until Element Is Visible    xpath=//body/div[@id='main-wrapper']/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/a[1]    10s
    Click Element    xpath=//body/div[@id='main-wrapper']/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/a[1]

    Comment    ตรวจสอบข้อความที่แสดงในหน้าจอ "รายการรอให้คำแนะนำ"
    Wait Until Element Is Visible    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/div[1]/div[1]/h3[1]/b    10s
    ${Listrecommend_screenname} =    Get Text    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/div[1]/div[1]/h3[1]/b
    Should Contain    ${Listrecommend_screenname}    รายการรอให้คำแนะนำ

    Comment    ตรวจสอบ input ที่หน้าจอ "รายการรอให้คำแนะนำ"
    Comment    ตรวจสอบ วันที่จากถึง
    Element Should Be Visible    xpath=//div[@id='reportrange']
    Comment    ตรวจสอบ combobox จังหวัด
    Element Should Be Visible    xpath=//select[@id='province']
    Comment    ตรวจสอบ combobox สถานพยาบาล
    Element Should Be Visible    xpath=//select[@id='hospital']
    Comment    ตรวจสอบ combobox กลุ่มโรค
    Element Should Be Visible    xpath=//select[@id='disease_group']
    Comment    ตรวจสอบ textbox textbox search
    Element Should Be Visible    xpath=//input[@id='txt_search']
    Comment    ตรวจสอบ btn ปุ่มค้นหา
    ${Listrecommend_btnsearch} =    Get Text    xpath=//button[contains(text(),'ค้นหา')]
    Should Be Equal    ค้นหา    ${Listrecommend_btnsearch}

    Comment    ตรวจสอบชื่อคอลัมน์ที่ตารางในหน้าจอ "รายการรอให้คำแนะนำ"
    ${Listrecommend_tblcolumn1} =    Get Text    xpath=//thead//tr/th[1]
    Should Be Equal    สถานพยาบาล    ${Listrecommend_tblcolumn1}
    ${Listrecommend_tblcolumn2} =    Get Text    xpath=//thead//tr/th[2]
    Should Be Equal    รหัสเข้ารับบริการ    ${Listrecommend_tblcolumn2}
    ${Listrecommend_tblcolumn3} =    Get Text    xpath=//thead//tr/th[3]
    Should Be Equal    วันเข้ารับบริการ    ${Listrecommend_tblcolumn3}
    ${Listrecommend_tblcolumn4} =    Get Text    xpath=//thead//tr/th[4]
    Should Be Equal    ชื่อ-สกุล    ${Listrecommend_tblcolumn4}
    ${Listrecommend_tblcolumn5} =    Get Text    xpath=//thead//tr/th[5]
    Should Contain    ${Listrecommend_tblcolumn5}    ความดัน
    ${Listrecommend_tblcolumn6} =    Get Text    xpath=//thead//tr/th[6]
    Should Contain    ${Listrecommend_tblcolumn6}    เบาหวาน
    ${Listrecommend_tblcolumn7} =    Get Text    xpath=//thead//tr/th[7]
    Should Contain    ${Listrecommend_tblcolumn7}    เบาหวานจอตา
    ${Listrecommend_tblcolumn8} =    Get Text    xpath=//thead//tr/th[8]
    Should Be Equal    ผิวหนัง    ${Listrecommend_tblcolumn8}
    ${Listrecommend_tblcolumn9} =    Get Text    xpath=//thead//tr/th[9]
    Should Be Equal    หัวใจ    ${Listrecommend_tblcolumn9}
    # ${Listrecommend_tblcolumn9} =    Get Text    xpath=//thead//tr/th[10]
    # Should Be Equal    Action    ${Listrecommend_tblcolumn9}

Verify search recommend record by vn no.
    [Arguments]    ${sleeptime}    ${vnno}

    Comment    รอหน้าเว็บโหลด กำลังโหลดข้อมูล ให้หายไป
    Wait Until Element Is Not Visible    xpath=//h3[contains(text(),'กำลังโหลดข้อมูล...')]    60s

    Comment    ระบุเงื่อนไขในการค้นหา VN
    Wait Until Element Is Visible    xpath=//input[@id='txt_search']    60s
    Input Text    xpath=//input[@id='txt_search']    ${vnno}

    Comment    กดปุ่ม ค้นหา
    Wait Until Element Is Visible    xpath=//button[contains(text(),'ค้นหา')]    10s
    Click element    xpath=//button[contains(text(),'ค้นหา')]

    Comment    ตรวจสอบผลลัพธ์รายการที่ได้จากการค้นหาข้อมูล
    Wait Until Element Is Visible    xpath=//body[1]/div[2]/div[1]/div[1]/div[2]/div[3]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[2]    20s
    ${searchresult_vnnumber} =    Get Text    xpath=//body[1]/div[2]/div[1]/div[1]/div[2]/div[3]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[2]
    Should Be Equal    ${vnno}    ${searchresult_vnnumber}

Choose list recommend record
    [Arguments]    ${sleeptime}

    Comment    เลือกกดปุ่ม ดูข้อมูล ใน record
    Wait Until Element Is Visible    xpath=//a[contains(text(),'ดูข้อมูล')]    10s
    Click Element    xpath=//a[contains(text(),'ดูข้อมูล')]

    Comment    สลับtab
    Switch Window    new
    Maximize Browser Window

Back to Recommend menu
    [Arguments]    ${sleeptime}    ${summary_vnnumbertrim}

    Comment    กดปุ่ม Back
    Wait Until Element Is Visible    xpath=//body[1]/div[2]/div[1]/div[1]/div[1]/ol[1]/li[2]/a[1]    10s
    Click element    xpath=//body[1]/div[2]/div[1]/div[1]/div[1]/ol[1]/li[2]/a[1]

    Comment    รอหน้าเว็บโหลด กำลังโหลดข้อมูล ให้หายไป
    Wait Until Element Is Not Visible    xpath=//h3[contains(text(),'กำลังโหลดข้อมูล...')]    60s
    Wait Until Element Is Visible    xpath=//input[@id='txt_search']    60s

    Comment    ใส่เลข VN ในช่องค้นหาอีกครั้ง
    Input Text    xpath=//input[@id='txt_search']    ${summary_vnnumbertrim}

Verify diagnose details in history record (pressure)
    [Arguments]    ${sleeptime}    ${diagnosis_data_type}    ${diagnosis_data_code}    ${diagnosis_data_additional}
    ...    ${template_detail}    ${diagnosis_guide}    ${userdiagnose_name}    ${config_firstgrader}

    Comment    เลือก tab ความดัน
    Wait Until Element Is Visible    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/div[2]/div[2]/ul[1]/li[2]/a[1]    10s
    Click Element    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/div[2]/div[2]/ul[1]/li[2]/a[1]
   
    Comment    ตรวจสอบข้อมูล ผู้วินิจฉัย
    Comment    กดดู Panel ข้อมูล การวินิจฉัย
    Wait Until Element Is Visible    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]    10s
    Click Element    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]

    Comment    ตรวจสอบข้อมูล ชื่อนามสกุลผู้วินิจฉัย
    Wait Until Element Is Visible    xpath=//body[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/h5[1]    10s
    ${summary_username_diagnose} =    Get Text    xpath=//body[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/h5[1]
    Should Contain    ${summary_username_diagnose}    ${userdiagnose_name}

    Comment    ตรวจสอบข้อมูลวินิจฉัย
    Comment    ตรวจสอบ คำวินิจฉัยจากแพทย์
    ${summary_label_diagnose} =    Get Text    xpath=//body[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/span[1]
    Should Be Equal    คำวินิจฉัยจากแพทย์ :    ${summary_label_diagnose}
    ${summary_type} =    Get Text    xpath=//body[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/small[1]
    Should Be Equal    ${diagnosis_data_type}    ${summary_type}
    ${summary_code} =    Get Text    xpath=//body[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/span[1]
    Should Contain    ${summary_code}    ${diagnosis_data_code}

    Comment    ตรวจสอบ คำวินิจฉัยจากแพทย์เพิ่มเติม
    ${summary_label_diagnoseadd1} =    Get Text    xpath=//body[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/span[1]
    Should Be Equal    คำวินิจฉัยจากแพทย์เพิ่มเติม :    ${summary_label_diagnoseadd1}
    ${summary_additional} =    Get Text    xpath=//body[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/p[1]
    Should Be Equal    ${diagnosis_data_additional}    ${summary_additional}
    
    Comment    ตรวจสอบ คำแนะนำจากแพทย์
    ${summary_label_suggestion} =    Get Text    xpath=//body[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[3]/span[1]
    Should Be Equal    คำแนะนำจากแพทย์ :    ${summary_label_suggestion}
    ${summary_detail} =    Get Text    xpath=//body[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/p[1]
    Should Be Equal    ${template_detail}    ${summary_detail}
    ${summary_label_guide} =    Get Text    xpath=//body[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[4]/span[1]
    
    Comment    ตรวจสอบ แนวทางการดูแล
    Should Be Equal    แนวทางการดูแล :    ${summary_label_guide}
    ${summary_guide} =    Get Text    xpath=//body[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[4]/div[1]/span[1]
    BuiltIn.Run Keyword If    '${config_firstgrader}'=='0' and '${diagnosis_guide}'=='อยู่ในเกณฑ์ปกติ'    Should Be Equal    ไม่ติดตามผล    ${summary_guide}
    BuiltIn.Run Keyword If    '${config_firstgrader}'=='0' and '${diagnosis_guide}'=='ติดตามผล'    Should Be Equal    ${diagnosis_guide}    ${summary_guide}
    BuiltIn.Run Keyword If    '${config_firstgrader}'=='0' and '${diagnosis_guide}'=='ติดตามผลและพบแพทย์ผ่านระบบทางไกล'    Should Contain    ${summary_guide}    ${diagnosis_guide}
    BuiltIn.Run Keyword If    '${config_firstgrader}'=='0' and '${diagnosis_guide}'=='ส่งตัวพบแพทย์'    Should Be Equal    ${diagnosis_guide}    ${summary_guide}
    BuiltIn.Run Keyword If    '${config_firstgrader}'=='1' and '${diagnosis_guide}'=='อยู่ในเกณฑ์ปกติ'    Should Be Equal    ไม่ติดตามผล    ${summary_guide}
    BuiltIn.Run Keyword If    '${config_firstgrader}'=='1' and '${diagnosis_guide}'=='ติดตามผล'    Should Be Equal    ${diagnosis_guide}    ${summary_guide}
    BuiltIn.Run Keyword If    '${config_firstgrader}'=='1' and '${diagnosis_guide}'=='ติดตามผลและพบแพทย์ผ่านระบบทางไกล'    Should Contain    ${summary_guide}    ${diagnosis_guide}
    BuiltIn.Run Keyword If    '${config_firstgrader}'=='1' and '${diagnosis_guide}'=='ส่งตัวพบแพทย์'    Should Be Equal    ${diagnosis_guide}    ${summary_guide}

New VN and insert primary data
    [Arguments]    ${sleeptime}   ${username_fornewvn}    ${password_fornewvn}
    ...    ${name_fornewvn}    ${profilepatient_data.prefixth}    ${profilepatient_data.nameth}    ${profilepatient_data.lastnameth}
    ...    ${profilepatient_data.idcard}    ${servicepressure.diabetes}    ${servicepressure.pressure}    ${servicepressure.sel_va}
    ...    ${servicepressure.com_eye}    ${servicepressure.eye}    ${servicepressure.ccd}    ${servicepressure.laser}
    ...    ${servicepressure.oct}    ${servicepressure.skin}    ${servicepressure.heart}    ${servicepressure.doctor}
    ...    ${pressure_on}    ${pressure_lower}    ${blood_pressure}    ${heartrate}    ${spo2}    ${typeblood_sugar}    ${blood_sugar}
    ...    ${temperature}    ${weight}    ${height}    ${waistline}

    Comment    เป็นการสร้าง VN ใหม่ + บันทึกข้อมูลสุขภาพ Tab สุขภาพเบื้องต้น
    Comment    ระบุ Username และ Password ที่ใช้เข้าสู่ระบบ
    OfficerLoginPage.Input valid username and password    ${sleeptime}    ${username_fornewvn}    ${password_fornewvn}
    Comment    ตรวจสอบหน้าจอ Landing Page (หน้าเมนูย่อย)
    OfficerLoginPage.Verify landing page    ${sleeptime}    ${name_fornewvn}
    Comment    เลือกเมนูย่อย ระบบคัดกรองโรค (Telehealth)
    OfficerLoginPage.Choose telehealth menu    ${sleeptime}
    Comment    ตรวจสอบเมนูและหน้าจอ ผู้เข้ารับบริการวันนี้
    ListDailyPage.Verify list daily screen    ${sleeptime}
    Comment    กดปุ่ม เพิ่มผู้เข้ารับบริการ
    ListDailyPage.Verify register button in list daily screen    ${sleeptime}
    Comment    ค้นหาผู้เข้ารับบริการ จาก ID Card และเลือกรายการที่ได้จากการค้นหา
    ListDailyPage.Choose patient    ${sleeptime}    ${profilepatient_data.idcard}
    Comment    เลือกรายการคัดกรอง + สรุปรายการคัดกรองที่บันทึก + ค้นหารายการที่เพิ่งเพิ่มเข้าไปในระบบที่เมนู "ผู้เข้ารับบริการวันนี้"
	ListDailyPage.Verify summary list    ${sleeptime}    ${profilepatient_data.prefixth}    ${profilepatient_data.nameth}
    ...    ${profilepatient_data.lastnameth}    ${servicepressure.diabetes}    ${servicepressure.pressure}    ${servicepressure.sel_va}
    ...    ${servicepressure.com_eye}    ${servicepressure.pressure_eye}    ${servicepressure.ccd}    ${servicepressure.laser}
    ...    ${servicepressure.oct}    ${servicepressure.skin}    ${servicepressure.heart}    ${servicepressure.doctor}

    Comment    เก็บเลข VN ที่เพิ่งสร้าง
    Wait Until Element Is Visible    xpath=//tbody/tr[1]/td[1]/a    10s
    ${summary_vnnumber} =    Get Text    xpath=//tbody/tr[1]/td[1]/a

    Comment    กดเลือกรายการ ผู้เข้ารับบริการวันนี้
    ListDailyPage.choose list daily record    ${sleeptime}
    Comment    บันทึกข้อมูลใน tab สุขภาพเบื้องต้น
    ListDailyPage.Insert data in "Primary" tab    ${sleeptime}    ${pressure_on}    ${pressure_lower}    ${blood_pressure}
    ...    ${heartrate}    ${spo2}    ${typeblood_sugar}    ${blood_sugar}    ${temperature}    ${weight}    ${height}    ${waistline}
    
    Comment    ส่งค่า เลข VN ที่สร้างใหม่กลับไป
    [Return]    ${summary_vnnumber}

Doctor choose recommend record and Add Template
    [Arguments]    ${sleeptime}   ${username_diagnose}    ${password_diagnose}    ${doctor_name}    ${newvnno}

    Comment    ปิด Browser
    CommonFunctionality.Closed Browser
    Comment    เปิด browser + เข้า URL telehealth
    CommonFunctionality.Start Test Case
    Comment    ระบุ Username และ Password ที่ใช้เข้าสู่ระบบ
    DoctorLoginPage.Input valid username and password    ${sleeptime}    ${username_diagnose}    ${password_diagnose}
    Comment    ตรวจสอบเมนูและหน้าจอ ผู้เข้ารับบริการวันนี้
    DoctorLoginPage.Verify landing page    ${sleeptime}    ${doctor_name}
    Comment    เลือกเมนูย่อย ระบบคัดกรองโรค (Telehealth)
    DoctorLoginPage.Choose telehealth menu    ${sleeptime}
    Comment    แพทย์ตรวจสอบ profile ตัวเองที่มุมขวามือ
    DoctorLoginPage.Verify doctor login profile    ${sleeptime}    ${doctor_name}
    Comment    แพทย์ตรวจสอบ 6 เมนูหลัก
    DoctorMainMenuPage.Verify all main menu    ${sleeptime}

    Comment    เข้าหน้าจอ เพิ่มเทมเพลต
    TemplatePage.Verify list template screen    ${sleeptime}
    Comment    เพิ่มเทมเพลตเพื่อใช้ในการทดสอบ
    TemplatePage.Add template record    ${sleeptime}    อาการไม่รุนแรง    พักผ่อนให้มากๆ
    Comment    กดปุ่ม ย้อนกลับ
    Wait Until Element Is Visible    xpath=//header/div[@id='h1']/nav[1]/div[1]/ul[1]/li[1]/a[1]/i[1]    10s
    Click Element    xpath=//header/div[@id='h1']/nav[1]/div[1]/ul[1]/li[1]/a[1]/i[1]
 
    Comment    กดเมนู รายการรอให้คำแนะนำ
    Verify list recommend screen    ${sleeptime}
    Comment    ค้นหา รายการรอให้คำแนะนำ จากเลข vn no.
    Verify search recommend record by vn no.    ${sleeptime}    ${newvnno}
    Comment    เลือก รายการรอให้คำแนะนำ 
    Choose list recommend record    ${sleeptime}

Add diagnosis (Tab pressure)
    [Arguments]    ${sleeptime}    ${diagnosis_data_type}    ${diagnosis_data_code}    ${diagnosis_data_additional}

    Comment    เลือก tab ความดัน
    Wait Until Element Is Visible    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/div[2]/div[2]/ul[1]/li[2]/a[1]    10s
    Click Element    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/div[2]/div[2]/ul[1]/li[2]/a[1]

    Comment    ตรวจสอบปุ่ม ประวัติคำวินิจฉัย + กดเลือก
    Wait Until Element Is Visible    xpath=//body[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/form[1]/div[1]/div[1]/div[1]/div[1]/div[1]/button[1]/span[1]    10s
    ${diagnosis_history} =    Get Text    xpath=//body[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/form[1]/div[1]/div[1]/div[1]/div[1]/div[1]/button[1]/span[1]
    Should Be Equal    ดูประวัติคำวินิจฉัย    ${diagnosis_history}

    Comment    ตรวจสอบ popup ประวัติคำวินิจฉัย
    Click Element    xpath=//body[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/form[1]/div[1]/div[1]/div[1]/div[1]/div[1]/button[1]/span[1]
    Wait Until Element Is Visible    xpath=//h5[@id='header_diag']    10s
    ${diagnosis_historyheader} =    Get Text    xpath=//h5[@id='header_diag']
    Should Be Equal    ประวัติคำวินิจฉัย (ความดัน)    ${diagnosis_historyheader}

    Comment    กดปุ่มปิด popup
    Sleep    ${sleeptime}
    Wait Until Element Is Visible    xpath=//body/div[@id='modalHistory']/div[1]/div[1]/div[1]/button[1]    10s
    Click Button    xpath=//body/div[@id='modalHistory']/div[1]/div[1]/div[1]/button[1]

    Comment    scroll down ไปล่างสุด
    execute Javascript    window.scrollTo(0,document.body.scrollHeight)

    Comment    ระบุ ประเภทคำวินิจฉัย
    Wait Until Element Is Visible    xpath=//select[@id='pressure_diagnosis']    10s
    Click element    xpath=//select[@id='pressure_diagnosis']
    Wait Until Element Is Visible   xpath=//option[contains(text(),'${diagnosis_data_type}')]    10s
    Select From List By Label    xpath=//select[@id='pressure_diagnosis']    ${diagnosis_data_type}

    Comment    ระบุ ประเภทคำวินิจฉัย เลือกจาก combobox
    Wait Until Element Is Visible    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/form[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[1]/span[1]/span[1]/span[1]     10s
    Click element    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/form[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[1]/span[1]/span[1]/span[1]
    Wait Until Element Is Visible    xpath=//body/span[1]/span[1]/span[1]/input[1]    10s
    Comment    ระบุ โค้ดคำวินิจฉัย
    Input Text    xpath=//body/span[1]/span[1]/span[1]/input[1]    ${diagnosis_data_code}

    Comment    หน่วงเวลารอรายการ icd 10
    Sleep    3s

    Comment    เลือก โค้ดคำวินิจฉัย
    Wait Until Element Is Visible    xpath=//ul[@id='select2-pressure_icd_code-results']/li    10s
    Click element    xpath=//ul[@id='select2-pressure_icd_code-results']/li

    Comment    กดบันทึก โค้ดคำวินิจฉัย
    Wait Until Element Is Visible    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/form[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[2]/button[1]    10s
    Click Button    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/form[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[2]/button[1]
    
    Comment   ตรวจสอบ โค้ดคำวินิจฉัย ที่เพิ่มไป
    Wait Until Element Is Visible    xpath=//table[@id='pressure_Table']/tbody/tr/td[1]    10s
    ${diagnosis_type} =    Get Text    xpath=//table[@id='pressure_Table']/tbody/tr/td[1]
    Should Be Equal    ${diagnosis_data_type}    ${diagnosis_type}
    ${diagnosis_code} =    Get Text    xpath=//table[@id='pressure_Table']/tbody/tr/td[2]
    Should Contain    ${diagnosis_code}    ${diagnosis_data_code}
  
    Comment    ระบุ คำวินิจฉัยเพิ่มเติม
    Wait Until Element Is Visible    xpath=//textarea[@id='additional_diagnose_2']    10s
    Input Text    xpath=//textarea[@id='additional_diagnose_2']    ${diagnosis_data_additional}

Add sugguestion (Tab pressure) and Edit Text from Template
    [Arguments]    ${sleeptime}    ${template_detail}

    Comment    กด combobox template
    Wait Until Element Is Visible    xpath=//select[@id='template1']    10s
    Click element    xpath=//select[@id='template1']
    Comment    กดเลือก template อาการไม่รุนแรง
    Wait Until Element Is Visible    xpath=//option[contains(text(),'อาการไม่รุนแรง')]    10s
    Select From List By Label    xpath=//select[@id='template1']    อาการไม่รุนแรง

    Comment    ดูว่า template ดึงข้อมูลมาถูกหรือไม่ ซึ่งเราได้สร้าง Template ไว้ก่อนหน้านี้แล้ว
    Wait Until Element Is Visible    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/form[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[3]/div[2]    10s
    ${sugguestion_detail} =    Get Text    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/form[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[3]/div[2]
    Should Be Equal    พักผ่อนให้มากๆ    ${sugguestion_detail}
    
    Comment    แก้ template ที่ดึงมา เป็นข้อความใหม่
    Wait Until Element Is Visible    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/form[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[3]/div[2]
    Input Text    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/form[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[3]/div[2]    ${template_detail}
    
Add guidelines by choose "Screening Next year" (Tab pressure)
    [Arguments]    ${sleeptime}

    Comment    scroll down ไปล่างสุด
    execute Javascript    window.scrollTo(0,document.body.scrollHeight)

    Comment    เลือก แนวทางการดูแล "อยู่ในเกณฑ์ปกติ"
    Wait Until Element Is Visible    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/form[1]/div[3]/div[1]/div[2]/div[1]/div[1]/label[1]    10s
    Click element   xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/form[1]/div[3]/div[1]/div[2]/div[1]/div[1]/label[1]

Submit diagnose and Verify confirm popup (Tab pressure)
    [Arguments]    ${sleeptime}    ${diagnosis_data_type}    ${diagnosis_data_code}    ${diagnosis_data_additional}
    ...    ${template_detail}    ${diagnosis_guide}    ${config_firstgrader}
    
    Comment    เก็บเลข VN ที่เลือกมาในเคสนี้
    Wait Until Element Is Visible    xpath=//body[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/h3[1]/b[1]    10s
    ${summary_vnnumber} =    Get Text    xpath=//body[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/h3[1]/b[1]
    ${summary_vnnumbertrim} =    Remove String    ${summary_vnnumber}    รหัสเข้ารับบริการ :    ${SPACE}

    Comment    กดปุ่ม บันทึกผลการตรวจ
    Wait Until Element Is Visible    xpath=//button[@id='b_summit']    10s
    Click Button    xpath=//button[@id='b_summit']

    Comment    ตรวจสอบ Popup ยืนยันบันทึกคำวินิจฉัย
    Wait Until Element Is Visible    xpath=//h4[@id='confirmModalLabel1']    10s
    ${summary_header} =    Get Text    xpath=//h4[@id='confirmModalLabel1']
    Should Be Equal    ยืนยันรายการตรวจ    ${summary_header}
    ${summary_label_diagnose} =    Get Text    xpath=//body/div[@id='modalConfirmt']/div[1]/div[1]/div[2]/form[1]/div[1]/label[1]
    Should Be Equal    คำวินิจฉัย:    ${summary_label_diagnose}
    ${summary_type} =    Get Text    xpath=//body/div[@id='modalConfirmt']/div[1]/div[1]/div[2]/form[1]/div[1]/div[1]/div[1]/small
    Should Be Equal    ${diagnosis_data_type}    ${summary_type}
    ${summary_code} =    Get Text    xpath=//body/div[@id='modalConfirmt']/div[1]/div[1]/div[2]/form[1]/div[1]/div[1]/div[1]/span[1]
    Should Contain    ${summary_code}    ${diagnosis_data_code}
    ${summary_label_diagnoseadd1} =    Get Text    xpath=//body[1]/div[6]/div[1]/div[1]/div[2]/form[1]/div[1]/label[2]
    Should Be Equal    คำวินิจฉัยเพิ่มเติม:    ${summary_label_diagnoseadd1}
    ${summary_label_diagnoseadd2} =    Get Text    xpath=//body[1]/div[6]/div[1]/div[1]/div[2]/form[1]/div[1]/div[2]/div[1]/small[1]
    Should Be Equal    คำวินิจฉัยเพิ่มเติม    ${summary_label_diagnoseadd2}
    ${summary_additional} =    Get Text    xpath=//body[1]/div[6]/div[1]/div[1]/div[2]/form[1]/div[1]/div[2]/div[1]/span[1]
    Should Be Equal    ${diagnosis_data_additional}    ${summary_additional}
    ${summary_label_suggestion} =    Get Text    xpath=//body[1]/div[6]/div[1]/div[1]/div[2]/form[1]/div[1]/label[3]
    Should Be Equal    คำแนะนำ:    ${summary_label_suggestion}
    ${summary_detail} =    Get Text    xpath=//p[@id='recommend_cf']
    Should Be Equal    ${template_detail}    ${summary_detail}
    ${summary_label_guide} =    Get Text    xpath=//body[1]/div[6]/div[1]/div[1]/div[2]/form[1]/div[1]/label[4]
    Should Be Equal    แนวทางการดูแล:    ${summary_label_guide}
    ${summary_guide} =    Get Text    xpath=//p[@id='note_cf']
    BuiltIn.Run Keyword If    '${config_firstgrader}'=='0' and '${diagnosis_guide}'=='อยู่ในเกณฑ์ปกติ'    Should Be Equal    ไม่ติดตามผล    ${summary_guide}
    BuiltIn.Run Keyword If    '${config_firstgrader}'=='0' and '${diagnosis_guide}'=='ติดตามผล'    Should Be Equal    ${diagnosis_guide}    ${summary_guide}
    BuiltIn.Run Keyword If    '${config_firstgrader}'=='0' and '${diagnosis_guide}'=='ติดตามผลและพบแพทย์ผ่านระบบทางไกล'    Should Contain    ${summary_guide}    ${diagnosis_guide}
    BuiltIn.Run Keyword If    '${config_firstgrader}'=='0' and '${diagnosis_guide}'=='ส่งตัวพบแพทย์'    Should Be Equal    ${diagnosis_guide}    ${summary_guide}
    
    Comment    ตรวจสอบปุ่มใน Popup ยืนยันบันทึกคำวินิจฉัย
    ${summary_btnclose} =    Get Text    xpath=//body[1]/div[6]/div[1]/div[1]/div[3]/button[1]
    Should Be Equal    ปิด    ${summary_btnclose}
    ${summary_btnsubmit} =    Get Text    xpath=//body/div[@id='modalConfirmt']/div[1]/div[1]/div[3]/button[2]
    Should Be Equal    บันทึกผลการตรวจ    ${summary_btnsubmit}

    Comment    ตกลงใน popup อีกครั้ง 
    Wait Until Element Is Visible    xpath=//body/div[@id='modalConfirmt']/div[1]/div[1]/div[3]/button[2]    10s
    Click Button    xpath=//body/div[@id='modalConfirmt']/div[1]/div[1]/div[3]/button[2]

    Comment    ตรวจสอบ popup หลังยืนยันบันทึกคำวินิจฉัย
    Comment    ตรวจสอบ ข้อความ บันทึกสำเร็จ
    Wait Until Element Is Visible    xpath=//div[@id='swal2-content']    10s
    ${summary_afterupdate} =    Get Text    xpath=//div[@id='swal2-content']
    Should Be Equal    บันทึกสำเร็จ    ${summary_afterupdate}
    Comment    ตรวจสอบ ปุ่ม OK
    Wait Until Element Is Visible    xpath=//button[contains(text(),'OK')]    10s
    ${summary_afterupdatebtn} =    Get Text    xpath=//button[contains(text(),'OK')]
    Should Be Equal    OK    ${summary_afterupdatebtn}

    Comment    กด OK
    Click Button    xpath=//button[contains(text(),'OK')]

    Comment    scroll down ไปบนสุด
    Execute JavaScript    window.scrollTo(0,0)

    Comment    ตรวจสอบ ประวัติคำวินิจฉัย หลังบันทึก
    Comment    กดปุ่ม popup ประวัติคำวินิจฉัย 
    Sleep    ${sleeptime}
    BuiltIn.Run Keyword If    '${config_firstgrader}'=='0'    Click Element    xpath=//body[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/form[1]/div[1]/div[1]/div[1]/div[1]/div[1]/button[1]/span[1]
    BuiltIn.Run Keyword If    '${config_firstgrader}'=='1'    Click Element    xpath=//body[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/form[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/button[1]/span[1]
    Wait Until Element Is Visible    xpath=//body/div[@id='modalHistory']/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]    10s
    Click Element    xpath=//body/div[@id='modalHistory']/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]
    
    Comment    ตรวจสอบ โค้ดวินิจฉัย ใน popup
    Wait Until Element Is Visible    xpath=//body[1]/div[5]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/span[1]    10s
    ${history_code} =    Get Text    xpath=//body[1]/div[5]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/span[1]
    Should Contain    ${history_code}    ${diagnosis_data_code}
    Comment    ตรวจสอบ คำวินิจฉัยเพิ่มเติม ใน popup
    ${history_additional} =    Get Text    xpath=//body[1]/div[5]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/span[1]
    Should Contain    ${history_additional}    ${diagnosis_data_additional}
    Comment    ตรวจสอบ คำแนะนำ ใน popup
    ${history_suggest} =    Get Text    xpath=//body[1]/div[5]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[3]/span[1]
    Should Contain    ${history_suggest}    ${template_detail}
    
    Comment    กดปิด popup
    Wait Until Element Is Visible    xpath=//body/div[@id='modalHistory']/div[1]/div[1]/div[1]/button[1]/span[1]/i[1]    10s
    Click Element    xpath=//body/div[@id='modalHistory']/div[1]/div[1]/div[1]/button[1]/span[1]/i[1]
    
    Comment    ย้อนกลับไปหน้า รายการรอให้คำแนะนำ เตรียมข้อมูลไว้ตรวจสอบขั้นตอนต่อไป
    Back to Recommend menu    ${sleeptime}    ${summary_vnnumbertrim}

Search vn record after diagnose (pressure) and Delete template
    [Arguments]    ${sleeptime}    ${diagnosis_data_type}    ${diagnosis_data_code}    ${diagnosis_data_additional}
    ...    ${template_detail}    ${username_fornewvn}    ${password_fornewvn}    ${name_fornewvn}
    ...    ${userdiagnose_name}    ${diagnosis_guide}    ${config_firstgrader}

    Comment    เก็บเลข VN 
    ${vnnumber} =    Get Value    xpath=//input[@id='txt_search']

    Comment    หมอหลังจากวินิจฉัย ค้นหารายการ VN ในหน้า ให้คำแนะนำ จะต้องไม่เจอ Record
    Comment    กดค้นหา
    Wait Until Element Is Visible    xpath=//button[contains(text(),'ค้นหา')]    10s
    Click element    xpath=//button[contains(text(),'ค้นหา')]

    Comment    รอผลการค้นหา  
    Wait Until Element Is Visible    xpath=//body[1]/div[2]/div[1]/div[1]/div[2]/div[3]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]    10s
    
    Comment    ผลการค้นหา จะต้องไม่เจอ Record
    ${search_notfound} =    Get Text    xpath=//body[1]/div[2]/div[1]/div[1]/div[2]/div[3]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]
    Should Be Equal    ไม่พบข้อมูล    ${search_notfound}
    
    Comment    หมอหลังจากวินิจฉัย ค้นหารายการ VN ในหน้า ประวัติ จะเจอ Record และมีข้อมูลคำวินิจฉัย
    Comment    กลับไปหน้าหลัก
    Wait Until Element Is Visible    xpath=//body[1]/div[2]/div[1]/div[1]/div[1]/ol[1]/li[1]/a[1]    10s
    Click Element    xpath=//body[1]/div[2]/div[1]/div[1]/div[1]/ol[1]/li[1]/a[1]
    Comment    ไปยังหน้าจอประวัติผู้เข้ารับบริการ
    DoctorListHistoryPage.Doctor verify list history screen    ${sleeptime}
    Comment    ค้นหา ประวัติผู้เข้ารับบริการ ด้วย Vn No
    DoctorListHistoryPage.Search history record by vnno    ${sleeptime}    ${vnnumber}
    Comment    กดเลือกรายการ ประวัติผู้เข้ารับบริการ
    DoctorListHistoryPage.Choose list history record    ${sleeptime}
    Comment    ตรวจสอบรายละเอียด ประวัติผู้เข้ารับบริการ ใน Tab ความดัน
    Verify diagnose details in history record (pressure)    ${sleeptime}    ${diagnosis_data_type}    ${diagnosis_data_code}
    ...    ${diagnosis_data_additional}    ${template_detail}    ${diagnosis_guide}    ${userdiagnose_name}
    ...    ${config_firstgrader}

    Comment    scroll down ไปบนสุด
    Execute JavaScript    window.scrollTo(0,0)

    Comment    กดย้อนกลับไปหน้าหลัก
    Wait Until Element Is Visible    xpath=//body[1]/div[2]/div[1]/div[1]/div[1]/ol[1]/li[1]/a[1]    10s
    Click element    xpath=//body[1]/div[2]/div[1]/div[1]/div[1]/ol[1]/li[1]/a[1]
    
    Comment    ไปยังเมนู จัดการเทมเพลต
    Verify list template screen    ${sleeptime}
    Comment    ค้นหาเทมเพลตจาก ขื่อเทมเพลต
    Verify search template record by topic name    ${sleeptime}    อาการไม่รุนแรง
    Comment    ลบรายการ เทมเพลต
    Delete template record    ${sleeptime}

    Comment    เจ้าหน้าที่ ค้นหารายการ VN ในหน้า ผู้ใช้บริการวันนี้ จะเจอ Record และไม่มีข้อมูลคำวินิจฉัย
    #fix ไม่จำเป็นต้องตรวจก็ได้ 
    # Comment    ระบุ Username และ Password ที่ใช้เข้าสู่ระบบ
    # DoctorLoginPage.Input valid username and password    ${sleeptime}    ${username.valid}    ${password.valid}
    # Comment    ตรวจสอบหน้าจอ Landing Page (หน้าเมนูย่อย)
    # DoctorLoginPage.Verify landing page    ${sleeptime}    ${name_userdiagnose}
    # Comment    เลือกเมนูย่อย ระบบคัดกรองโรค (Telehealth)
    # DoctorLoginPage.Choose telehealth menu    ${sleeptime}
    # Comment    ตรวจสอบ Profile ของผู้เข้าสู่ระบบ (doctor)
    # DoctorLoginPage.Verify doctor login profile    ${sleeptime}    ${name_userdiagnose}
    # Comment    ตรวจสอบ 6 เมนูหลักของพนักงาน
    # DoctorMainMenuPage.Verify all main menu    ${sleeptime}	
    # Comment    ไปยังเมนู รายการรอให้คำแนะนำ
    # RecommendPage.Verify list recommend screen    ${sleeptime}

    Comment    เจ้าหน้าที่ ค้นหารายการ VN ในหน้า ประวัติ จะเจอ Record และมีข้อมูลคำวินิจฉัย
    Comment    ปิด Browser
    CommonFunctionality.Closed Browser
    Comment    Comment    เปิด browser + เข้า URL telehealth
    CommonFunctionality.Start Test Case
    Comment    ระบุ Username และ Password ที่ใช้เข้าสู่ระบบ
    OfficerLoginPage.Input valid username and password    ${sleeptime}    ${username_fornewvn}    ${password_fornewvn}
    Comment    ตรวจสอบหน้าจอ Landing Page (หน้าเมนูย่อย)
    OfficerLoginPage.Verify landing page    ${sleeptime}    ${name_fornewvn}
    Comment    เลือกเมนูย่อย ระบบคัดกรองโรค (Telehealth)
    OfficerLoginPage.Choose telehealth menu    ${sleeptime}
    Comment    ตรวจสอบ Profile ของผู้เข้าสู่ระบบ (Officer)
    OfficerLoginPage.Verify officer login profile    ${sleeptime}    ${name_fornewvn}
    Comment    ตรวจสอบเมนูและหน้าจอ ประวัติผู้เข้ารับบริการ
	OfficerListHistoryPage.Officer verify list history screen    ${sleeptime}
    Comment    ค้นหา ประวัติผู้เข้ารับบริการ ด้วย VN No.
    OfficerListHistoryPage.Search history record by vnno    ${sleeptime}    ${vnnumber}
    Comment    ตรวจข้อมูลที่ได้จากการค้นหา
    OfficerListHistoryPage.Choose list history record    ${sleeptime}
    Comment    ตรวจข้อมูลวินิจฉัยที่กรอกมา Tab ใหม่
    Verify diagnose details in history record (pressure)    ${sleeptime}    ${diagnosis_data_type}    ${diagnosis_data_code}
    ...    ${diagnosis_data_additional}    ${template_detail}    ${diagnosis_guide}    ${userdiagnose_name}
    ...    ${config_firstgrader}


































































