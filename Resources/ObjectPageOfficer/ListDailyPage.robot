*** Settings  ***
# นำเข้า Libary ที่จะใช้งาน
Library    Selenium2Library
Library    String
Library    DateTime

*** Keywords ***
Verify list daily screen
    [Arguments]    ${sleeptime}

    Comment    Refresh Browser โหลดหน้าจอใหม่
    Reload Page

    Comment    กดปุ่ม "ผู้เข้ารับบริการวันนี้"
	Wait Until Element Is Visible    xpath=//body/div[@id='main-wrapper']/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]    100s
    Click Element    xpath=//body/div[@id='main-wrapper']/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]

    Comment    ตรวจสอบข้อความที่แสดงในหน้าจอ "ผู้เข้ารับบริการวันนี้"
    Wait Until Element Is Visible    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/div[1]/div[1]/h3[1]/b    10s
    ${listdaily_screenname} =    Get Text    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/div[1]/div[1]/h3[1]/b
    Should Contain    ${listdaily_screenname}    ผู้เข้ารับบริการวันนี้

    Comment    ตรวจสอบ input ที่หน้าจอ "ผู้เข้ารับบริการวันนี้"
    Wait Until Element Is Visible    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/form[1]/div[1]/div[1]/input[1]    10s
    Element Should Be Visible    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/form[1]/div[1]/div[1]/input[1]
    Comment    ตรวจสอบ ปุ่ม เพิ่มผู้เข้ารับบริการค้นหา
    Element Should Be Visible    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/form[1]/div[1]/div[2]/button
    ${listdaily_btnsearch} =    Get Text    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/form[1]/div[1]/div[2]/button
    Should Be Equal    ค้นหา    ${listdaily_btnsearch}
    Comment    ตรวจสอบ ปุ่ม เพิ่มผู้เข้ารับบริการ
    Element Should Be Visible    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/form[1]/div[1]/div[3]/a[1]/span
    ${listdaily_btnnew} =    Get Text    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/form[1]/div[1]/div[3]/a[1]/span
    Should Be Equal    เพิ่มผู้เข้ารับบริการ    ${listdaily_btnnew}

    Comment    ตรวจสอบชื่อคอลัมน์ที่ตารางในหน้าจอ "ผู้เข้ารับบริการวันนี้"
    ${listdaily_tblcolumn1name} =    Get Text    xpath=//table[@id='myTable']/thead/tr/th[1]
    Should Be Equal    รหัสเข้ารับบริการ    ${listdaily_tblcolumn1name}
    ${listdaily_tblcolumn2name} =    Get Text    xpath=//table[@id='myTable']/thead/tr/th[2]
    Should Be Equal    วัน-เวลา ออกคิว    ${listdaily_tblcolumn2name}
    ${listdaily_tblcolumn3name} =    Get Text    xpath=//table[@id='myTable']/thead/tr/th[3]
    Should Be Equal    ชื่อ-สกุล    ${listdaily_tblcolumn3name}
    ${listdaily_tblcolumn4name} =    Get Text    xpath=//table[@id='myTable']/thead/tr/th[4]
    Should Be Equal    สถานะ    ${listdaily_tblcolumn4name}

Verify register button in list daily screen
    [Arguments]    ${sleeptime}

    Comment    ตรวจสอบปุ่ม เพื่มผุ้เข้ารับบริการ ในหน้าจอ "ผู้เข้ารับบริการวันนี้"
    Wait Until Element Is Visible    xpath=//*[@id='main-wrapper']/div[1]/div[1]/div[2]/form[1]/div[1]/div[3]/a    10s
    Click Element    xpath=//*[@id='main-wrapper']/div[1]/div[1]/div[2]/form[1]/div[1]/div[3]/a

Choose patient
    [Arguments]    ${sleeptime}    ${idcard}

    Comment    ระบุเงื่อนไขในการค้นหา
    Wait Until Element Is Visible    xpath=//*[@id='search_data']    10s
    Input Text    xpath=//*[@id='search_data']    ${idcard}

    Comment    กดปุ่มค้นหา
    Wait Until Element Is Visible    xpath=//*[@id='main-wrapper']/div[1]/div[1]/div[2]/form[1]/div[1]/div[2]/Button    10s
    Click Button    xpath=//*[@id='main-wrapper']/div[1]/div[1]/div[2]/form[1]/div[1]/div[2]/Button

    Comment    กด เลือกรายการ
    Wait Until Element Is Visible    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/div[2]/div[1]/a[1]    10s
    Click Element    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/div[2]/div[1]/a[1]
    Wait Until Element Is Visible    xpath=//*[@id='main-wrapper']/div[1]/div[1]/div[2]/div[2]/div[4]/button    20s
    Click Button    xpath=//*[@id='main-wrapper']/div[1]/div[1]/div[2]/div[2]/div[4]/button

Verify summary list
    [Arguments]    ${sleeptime}    ${prefixth}    ${nameth}    ${lastnameth}    ${diabetes}    ${pressure}    ${sel_va}    ${com_eye}
    ...    ${pressure_eye}    ${ccd}    ${laser}    ${oct}    ${skin}    ${heart}    ${doctor}    

    Comment    ตรวจสอบข้อความที่แสดงในหน้าจอ "รายการคัดกรอง"
    Wait Until Element Is Visible    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/form[1]/div[1]/div[2]/button    10s
    Element Should Contain    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/div[1]/div[1]/h3[1]/b    รายการคัดกรอง
    
    Comment    ตรวจสอบปุ่มในหน้าจอ "รายการคัดกรอง"
    Element Should Be Visible    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/form[1]/div[1]/div[2]/button
    ${chooseservice_btnnext} =    Get Text    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/form[1]/div[1]/div[2]/button
    Should Be Equal    ขั้นตอนต่อไป    ${chooseservice_btnnext}

    Comment    เลือกรายการคัดกรองทั้งหมด โดย production ไม่มี ตรวจวัดระดับสายตา , คัดกรองหัวใจ
    Wait Until Element Is Visible    xpath=//body[1]/div[2]/div[1]/div[1]/div[2]/form[1]/div[1]/div[1]/div[1]/ul[1]/li[1]/b[1]    10s
    ${isCheck_diabetes}=     Run Keyword And Return Status    Element Should Be Visible    xpath=//b[contains(text(),'คัดกรองเบื้องต้น (เบาหวาน)')]
    BuiltIn.Run Keyword If    '${isCheck_diabetes}'=='True' and '${diabetes}'=='1'     Click Element    xpath=//b[contains(text(),'คัดกรองเบื้องต้น (เบาหวาน)')]
    ${isCheck_pressure}=     Run Keyword And Return Status    Element Should Be Visible    xpath=//b[contains(text(),'คัดกรองเบื้องต้น (ความดัน)')]
    BuiltIn.Run Keyword If    '${isCheck_pressure}'=='True' and '${pressure}'=='1'     Click Element    xpath=//b[contains(text(),'คัดกรองเบื้องต้น (ความดัน)')]
    # ${isCheck_sel_va}=     Run Keyword And Return Status    Element Should Be Visible    xpath=//b[contains(text(),'ตรวจวัดระดับสายตา')]
    # BuiltIn.Run Keyword If    '${isCheck_sel_va}'=='True' and '${sel_va}'=='1'     Click Element    xpath=//b[contains(text(),'ตรวจวัดระดับสายตา')]
    ${isCheck_com_eye}=     Run Keyword And Return Status    Element Should Be Visible    xpath=//b[contains(text(),'ตรวจค่าสายตา')]
    BuiltIn.Run Keyword If    '${isCheck_com_eye}'=='True' and '${com_eye}'=='1'     Click Element    xpath=//b[contains(text(),'ตรวจค่าสายตา')]
    ${isCheck_pressure_eye}=     Run Keyword And Return Status    Element Should Be Visible    xpath=//b[contains(text(),'ตรวจค่าความดันตา')]
    BuiltIn.Run Keyword If    '${isCheck_pressure_eye}'=='True' and '${pressure_eye}'=='1'     Click Element    xpath=//b[contains(text(),'ตรวจค่าความดันตา')]
    ${isCheck_ccd}=     Run Keyword And Return Status    Element Should Be Visible    xpath=//b[contains(text(),'ถ่ายภาพจอตา (CCD)')]
    BuiltIn.Run Keyword If    '${isCheck_ccd}'=='True' and '${ccd}'=='1'     Click Element    xpath=//b[contains(text(),'ถ่ายภาพจอตา (CCD)')]
    ${isCheck_laser}=     Run Keyword And Return Status    Element Should Be Visible    xpath=//b[contains(text(),'ถ่ายภาพจอตา (Laser)')]
    BuiltIn.Run Keyword If    '${isCheck_laser}'=='True' and '${laser}'=='1'     Click Element    xpath=//b[contains(text(),'ถ่ายภาพจอตา (Laser)')]
    ${isCheck_oct}=     Run Keyword And Return Status    Element Should Be Visible    xpath=//b[contains(text(),'ถ่ายภาพตัดขวางจอตา')]
    BuiltIn.Run Keyword If    '${isCheck_oct}'=='True' and '${oct}'=='1'     Click Element    xpath=//b[contains(text(),'ถ่ายภาพตัดขวางจอตา')]
    ${isCheck_skin}=     Run Keyword And Return Status    Element Should Be Visible    xpath=//b[contains(text(),'ถ่ายภาพอาการผิวหนัง')]
    BuiltIn.Run Keyword If    '${isCheck_skin}'=='True' and '${skin}'=='1'     Click Element    xpath=//b[contains(text(),'ถ่ายภาพอาการผิวหนัง')]
    # ${isCheck_heart}=     Run Keyword And Return Status    Element Should Be Visible    xpath=//b[contains(text(),'คัดกรองหัวใจ')]
    # BuiltIn.Run Keyword If    '${isCheck_heart}'=='True' and '${heart}'=='1'     Click Element    xpath=//b[contains(text(),'คัดกรองหัวใจ')]
    ${isCheck_doctor}=     Run Keyword And Return Status    Element Should Be Visible    xpath=//b[contains(text(),'นัดพบแพทย์ (ไม่มี)')]
    BuiltIn.Run Keyword If    '${isCheck_doctor}'=='True' and '${doctor}'=='1'     Click Element    xpath=//b[contains(text(),'นัดพบแพทย์ (ไม่มี)')]
    
    comment    กดปุ่ม ขั้นตอนต่อไป
    Wait Until Element Is Visible    xpath=//*[@id='main-wrapper']/div[1]/div[1]/div[2]/form[1]/div[1]/div[2]/button    10s
    Click Button    xpath=//*[@id='main-wrapper']/div[1]/div[1]/div[2]/form[1]/div[1]/div[2]/button

    Comment    ตรวจสอบรายละเอียดผู้เข้ารับบริการ ที่แสดงในหน้าจอ
    Wait Until Element Is Visible    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/div[1]/div[1]/h4[1]/b    10s
    Wait Until Element Is Visible    xpath=//body[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/h3[1]/b[1]    15s
    ${summary_queue} =    Get Text    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/div[1]/div[1]/h4[1]/b
    Should Contain    ${summary_queue}    คิวที่
    ${summary_vnnumber} =    Get Text    xpath=//body[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/h3[1]/b[1]
    Should Contain    ${summary_vnnumber}    VN
    Capture Page Screenshot
    ${summary_fullname} =    Get Text    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/div[2]/div[2]/h3
    Should Contain    ${summary_fullname}    ${prefixth}
    Should Contain    ${summary_fullname}    ${nameth}
    Should Contain    ${summary_fullname}    ${lastnameth}
    ${summary_alllistnumber} =    Get Text    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/div[2]/div[2]/h4[1]/b
    
    Comment    ตรวจสอบรายละเอียดตัวเลือกการตรวจ ที่แสดงในหน้าจอ
    Should Contain    ${summary_alllistnumber}    รายการตรวจทั้งหมด
    BuiltIn.Run Keyword If    '${diabetes}'=='1'    Page Should Contain    คัดกรองเบื้องต้น (เบาหวาน)
    BuiltIn.Run Keyword If    '${pressure}'=='1'    Page Should Contain    คัดกรองเบื้องต้น (ความดัน)
    BuiltIn.Run Keyword If    '${sel_va}'=='1'    Page Should Contain    ตรวจวัดระดับสายตา
    BuiltIn.Run Keyword If    '${com_eye}'=='1'    Page Should Contain    ตรวจค่าสายตา
    BuiltIn.Run Keyword If    '${pressure_eye}'=='1'    Page Should Contain    ตรวจค่าความดันตา
    BuiltIn.Run Keyword If    '${ccd}'=='1'    Page Should Contain    ถ่ายภาพจอตา (CCD)
    BuiltIn.Run Keyword If    '${laser}'=='1'    Page Should Contain    ถ่ายภาพจอตา (Laser)
    BuiltIn.Run Keyword If    '${oct}'=='1'    Page Should Contain    ถ่ายภาพตัดขวางจอตา
    BuiltIn.Run Keyword If    '${skin}'=='1'    Page Should Contain    ถ่ายภาพอาการผิวหนัง
    BuiltIn.Run Keyword If    '${heart}'=='1'    Page Should Contain    คัดกรองหัวใจ
    BuiltIn.Run Keyword If    '${doctor}'=='1'    Page Should Contain    นัดพบแพทย์
    ${summary_btn} =    Get Text    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/div[2]/div[3]/a[1]/span[1]/u
    Should Be Equal    กลับสู่หน้าเพิ่มผู้เข้ารับบริการ    ${summary_btn}

    Comment    เก็บค่าเลข VN
    ${summary_vnnumbertrim} =    Remove String    ${summary_vnnumber}    VN    ${SPACE}

    Comment    ย้อนกลับไปหน้าจอ "ผู้เข้ารับบริการวันนี้" เพื่อค้นหารายการที่เพิ่งเพิ่มเข้าไปในระบบ
    Wait Until Element Is Visible    xpath=//*[@id='main-wrapper']/div[1]/div[1]/div[2]/div[2]/div[3]/a    10s
    Click Element    xpath=//*[@id='main-wrapper']/div[1]/div[1]/div[2]/div[2]/div[3]/a
    Wait Until Element Is Visible    xpath=//*[@id='h1']/nav[1]/div[1]/ul[1]/li[1]/a    10s
    Click Element    xpath=//*[@id='h1']/nav[1]/div[1]/ul[1]/li[1]/a

    Comment    ใส่เงื่อนไข ค้นหารายการที่เพิ่งเพิ่มเข้าไปในระบบ
    Wait Until Element Is Visible    xpath=//*[@id='main-wrapper']/div[1]/div[1]/div[2]/form[1]/div[1]/div[2]/button    10s
    Input Text    xpath=//*[@id='main-wrapper']/div[1]/div[1]/div[2]/form[1]/div[1]/div[1]/input    ${summary_vnnumbertrim}
    Click Button    xpath=//*[@id='main-wrapper']/div[1]/div[1]/div[2]/form[1]/div[1]/div[2]/button

    Comment    ตรวจสอบผลลัพธ์รายการที่ได้จากการค้นหาข้อมูล
    Wait Until Element Is Visible    xpath=//table[@id='myTable']/tbody/tr[1]/td[1]    20s
    ${searchresult_vnnumber} =    Get Text    xpath=//table[@id='myTable']/tbody/tr[1]/td[1]
    Should Be Equal    ${summary_vnnumbertrim}    ${searchresult_vnnumber}
    ${searchresult_time} =    Get Text    xpath=//table[@id='myTable']/tbody/tr[1]/td[2]
    #Should Contain    ${searchresult_time}    เม.ย.
    ${searchresult_fullname} =    Get Text    xpath=//table[@id='myTable']/tbody/tr[1]/td[3]
    Should Contain    ${searchresult_fullname}    ${prefixth}  ${nameth}  ${lastnameth}
    ${searchresult_teleno} =    Get Text    xpath=//table[@id='myTable']/tbody/tr[1]/td[3]
    Should Contain    ${searchresult_teleno}    Tele HN
    ${searchresult_status} =    Get Text    xpath=//tbody/tr[1]/td[4]/span[1]/button
    Should Be Equal    อยู่ระหว่างตรวจ    ${searchresult_status}
    ${searchresult_more} =    Get Text    xpath=//tbody/tr[1]/td[5]/a
    Should Be Equal    ดูข้อมูล    ${searchresult_more}
    Mouse Over    xpath=//tbody/tr[1]/td[4]/span[1]/button
    Page Should Contain    สถานะการเข้ารับบริการ
    Page Should Contain    คัดกรอง
    Page Should Contain    รายการ
    BuiltIn.Run Keyword If    '${diabetes}'=='1'    Page Should Contain    คัดกรองเบื้องต้น (เบาหวาน)
    BuiltIn.Run Keyword If    '${pressure}'=='1'    Page Should Contain    คัดกรองเบื้องต้น (ความดัน)
    BuiltIn.Run Keyword If    '${sel_va}'=='1'    Page Should Contain    วัดระดับสายตา
    BuiltIn.Run Keyword If    '${com_eye}'=='1'    Page Should Contain    ตรวจค่าสายตา
    BuiltIn.Run Keyword If    '${skin}'=='1'    Page Should Contain    ภาพอาการผิวหนัง

    Comment    เก็บค่าวันที่ทำรายการ
    ${summary_dateth} =    Get Text    xpath=//body[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/h3[1]/b[1]
    ${summary_datethtrim} =    Remove String    ${summary_dateth}    ผู้เข้ารับบริการวันนี้ (    )

    [Return]    ${summary_vnnumbertrim}    ${summary_datethtrim}

choose list daily record
    [Arguments]    ${sleeptime}

    Comment    เลือกกดปุ่มดูข้อมูลใน record
    Wait Until Element Is Visible    xpath=//tbody/tr[1]/td[5]/a    10s
    Click Element    xpath=//tbody/tr[1]/td[5]/a

    Comment    สลับ tab ไปยัง Tab ใหม่
    Switch Window    new

Insert data in "Primary" tab
    [Arguments]    ${sleeptime}    ${pressure_on}    ${pressure_lower}    ${blood_pressure}    ${heartrate}    ${spo2}    ${typeblood_sugar}
    ...    ${blood_sugar}    ${temperature}    ${weight}    ${height}    ${waistline}
    
    Comment    เลือก tab สุขภาพเบื้องต้น
    Wait Until Element Is Visible    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/div[2]/div[2]/ul[1]/li[1]/a[1]    10s
    Click Element    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/div[2]/div[2]/ul[1]/li[1]/a[1]

    Comment    กดปุ่มแก้ไขผลการตรวจ
    Wait Until Element Is Visible    xpath=//button[@id='btn_editBasic']    10s
    Click Element    xpath=//button[@id='btn_editBasic']
    
    Comment    ระบุข้อมูลในหน้าจอ
    Comment    ระบุข้อมูล ความดันตัวบน
    Wait Until Element Is Visible    xpath=//input[@id='pressure_on']    10s
    Input Text    xpath=//input[@id='pressure_on']    ${pressure_on}
    Comment    ระบุข้อมูล ความดันตัวล่าง
    Input Text    xpath=//input[@id='pressure_lower']    ${pressure_lower}
    Comment    ระบุข้อมูล ค่าความดันเลือดแดงเฉลี่ย
    Input Text    xpath=//input[@id='blood_pressure']    ${blood_pressure}
    Comment    ระบุข้อมูล อัตราการเต้นของหัวใจ
    Input Text    xpath=//input[@id='heartrate']    ${heartrate}
    Comment    ระบุข้อมูล ค่าความอิ่มตัวของออกซิเจนในเลือด SPO2
    Input Text    xpath=//input[@id='spo2']    ${spo2}
    Comment    เลือกประเภท น้ำตาลในเลือด
    Click Element   xpath=//label[contains(text(),'${typeblood_sugar}')]
    Comment    ระบุข้อมูล น้ำตาลในเลือด
    Input Text    xpath=//input[@id='blood_sugar']    ${blood_sugar}
    Comment    ระบุข้อมูล อุณหภูมิร่างกาย
    Input Text    xpath=//input[@id='temperature']    ${temperature}
    Comment    ระบุข้อมูล น้ำหนัก
    Input Text    xpath=//input[@id='weight']    ${weight}
    Comment    ระบุข้อมูล ส่วนสูง
    Input Text    xpath=//input[@id='height']    ${height}
    Comment    ระบุข้อมูล เส้นรอบเอว
    Input Text    xpath=//input[@id='waistline']    ${waistline}

    Comment    ตรวจสอบปุ่ม ยกเลิก และ บันทึกและคัดกรองผล
    Wait Until Element Is Visible    xpath=//button[@id='btn_cancelBasic']    10s
    Element Should Be Visible    xpath=//button[@id='btn_cancelBasic']
    ${tabprimary_btncancelbasic} =    Get Text    xpath=//button[@id='btn_cancelBasic']
    Should Be Equal    ยกเลิก    ${tabprimary_btncancelbasic}
    Element Should Be Visible    xpath=//button[@id='btn_saveBasic']
    ${tabprimary_btnsavebasic} =    Get Text    xpath=//button[@id='btn_saveBasic']
    Should Be Equal    บันทึกและคัดกรองผล    ${tabprimary_btnsavebasic}

    Comment    กดปุ่มบันทึก
    Wait Until Element Is Visible    xpath=//button[@id='btn_saveBasic']    10s
    Click Button    xpath=//button[@id='btn_saveBasic']

    Comment    ตรวจสอบ pop up หลังจากกดปุ่มบันทึก
    Wait Until Element Is Visible    xpath=//div[@id='swal2-content']    10s
    ${tabprimary_msgaftersubmit} =    Get Text    xpath=//div[@id='swal2-content']
    Should Be Equal    บันทึกข้อมูลเบื้องต้นเรียบร้อยแล้วค่ะ    ${tabprimary_msgaftersubmit}
    Wait Until Element Is Visible    xpath=//button[contains(text(),'OK')]    10s
    ${tabprimary_btnaftersubmit} =    Get Text    xpath=//button[contains(text(),'OK')]
    Should Be Equal    OK    ${tabprimary_btnaftersubmit}

    Comment    กดok ใน pop up
    Wait Until Element Is Visible    xpath=//button[contains(text(),'OK')]    10s
    Click Button    xpath=//button[contains(text(),'OK')]
    
    Comment    ทดสอบกดปุ่ม แก้ไขผลการตรวจ แล้วกด ปุ่มยกเลิก ต่อ
    Wait Until Element Is Visible    xpath=//button[@id='btn_editBasic']    10s
    Click Button    xpath=//button[@id='btn_editBasic']
    Wait Until Element Is Visible    xpath=//button[@id='btn_cancelBasic']    10s
    Click Button    xpath=//button[@id='btn_cancelBasic']
    Element Should Not Be Visible    xpath=//button[@id='btn_cancelBasic']











































