*** Settings  ***
# นำเข้า Libary ที่จะใช้งาน
Library    Selenium2Library
Library    String
Library    DateTime

*** Keywords ***
Officer verify list history screen
    [Arguments]    ${sleeptime}

    Comment    กดปุ่ม "ประวัติผู้เข้ารับบริการ"
	Wait Until Element Is Visible    xpath=//body/div[@id='main-wrapper']/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[3]/a    10s
    Click Element    xpath=//body/div[@id='main-wrapper']/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[3]/a

    Comment    ตรวจสอบข้อความที่แสดงในหน้าจอ "ประวัติผู้เข้ารับบริการ"
    Wait Until Element Is Visible    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/div[1]/div[1]/h3[1]/b    10s
    ${listhistory_screenname} =    Get Text    xpath=//body/div[@id='main-wrapper']/div[1]/div[1]/div[2]/div[1]/div[1]/h3[1]/b
    Should Contain    ${listhistory_screenname}    ประวัติผู้เข้ารับบริการ

    Comment    ตรวจสอบ input ที่หน้าจอ "ประวัติผู้เข้ารับบริการ"
    Comment    ปุ่ม ปฏิทิน
    Element Should Be Visible    xpath=//div[@id='reportrange3']
    Comment    ปุ่ม Textbox search
    Element Should Be Visible    xpath=//*[@id='txt_search']
    Comment    ปุ่ม ค้นหา
    Element Should Be Visible    xpath=//*[@id='bt_get_report']
    ${listhistory_btnname} =    Get Text    xpath=//*[@id='bt_get_report']
    Should Be Equal    ค้นหา    ${listhistory_btnname}
    Comment    ปุ่ม Export
    Element Should Be Visible    xpath=//*[@id='btn_export_modal']
    ${listhistory_btnname} =    Get Text    xpath=//*[@id='btn_export_modal']
    Should Be Equal    Export    ${listhistory_btnname}

    Comment    ตรวจสอบชื่อคอลัมน์ที่ตารางในหน้าจอ "ประวัติผู้เข้ารับบริการ"
    ${listhistory_tblcolumn1name} =    Get Text    xpath=//th[text()='ลำดับที่']
    Should Be Equal    ลำดับที่    ${listhistory_tblcolumn1name}
    ${listhistory_tblcolumn2name} =    Get Text    xpath=//th[text()='รหัสเข้ารับบริการ']
    Should Be Equal    รหัสเข้ารับบริการ    ${listhistory_tblcolumn2name}
    ${listhistory_tblcolumn3name} =    Get Text    xpath=//th[text()='วัน-เวลาตรวจ']
    Should Be Equal    วัน-เวลาตรวจ    ${listhistory_tblcolumn3name}
    ${listhistory_tblcolumn4name} =    Get Text    xpath=//th[text()='ชื่อ-สกุล']
    Should Be Equal    ชื่อ-สกุล    ${listhistory_tblcolumn4name}
    ${listhistory_tblcolumn5name} =    Get Text    xpath=//th[text()='สถานะ']
    Should Be Equal    สถานะ    ${listhistory_tblcolumn5name}
    # ${listhistory_tblcolumn6name} =    Get Text    xpath=//table[@id='table_vn']/thead/tr/th[6]
    # Should Be Equal    Action    ${listhistory_tblcolumn6name}

Search history record by vnno
    [Arguments]    ${sleeptime}    ${input_vnno}

    Comment    รอหน้าเว็บโหลดข้อมูล
    Wait Until Element Is Not Visible    xpath=//h3[contains(text(),'กำลังโหลดข้อมูล...')]    20s

    Comment    ระบุใส่คำค้นหา vnno
    Wait Until Element Is Visible    xpath=//*[@id='txt_search']     10s
    Input Text    xpath=//*[@id='txt_search']    ${input_vnno}

    Comment    กดปุ่ม ค้นหา
    Wait Until Element Is Visible    xpath=//*[@id='bt_get_report']     10s
    Click Button    xpath=//*[@id='bt_get_report']

    Comment    รอหน้าเว็บโหลดข้อมูล
    Wait Until Element Is Not Visible    xpath=//h3[contains(text(),'กำลังโหลดข้อมูล...')]    15s

    Comment    ตรวจสอบผลลัพธ์ที่ได้ ในหน้าจอ "ประวัติผู้เข้ารับบริการ"
    Wait Until Element Is Visible    xpath=//tbody/tr[1]/td[2]    10s
    ${result_vnno} =    Get Text    xpath=//tbody/tr[1]/td[2]
    Should Be Equal    ${input_vnno}    ${result_vnno}

Choose list history record
    [Arguments]    ${sleeptime}

    Comment    เลือกกดปุ่มดูข้อมูลใน record
    Wait Until Element Is Visible    xpath=//html/body/div[2]/div/div/div[2]/div[3]/div/div/div/div[2]/div/table/tbody/tr/td[6]/a    10s
    Click Element    xpath=//html/body/div[2]/div/div/div[2]/div[3]/div/div/div/div[2]/div/table/tbody/tr/td[6]/a

    Comment    สลับ tab ไปยัง Tab ใหม่
    Switch Window    new
