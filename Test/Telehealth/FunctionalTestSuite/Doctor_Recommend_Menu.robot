*** Settings ***
# Settings ส่วนของการตั้งค่าให้กับไฟล์ Robot ของเรา ไม่ว่าจะเป็นการ Import Library, Resource หรือแม้กระทั่ง Setup คำสั่งต่างๆ ก่อนเริ่มทดสอบ
Library  Selenium2Library

Documentation    Menu "รายการรอให้คำแนะนำ"
Resource   ../../../Resources/CommonFunctionality.robot 
Resource   ../../../Resources/ObjectPageDoctor/DoctorLoginPage.robot
Resource   ../../../Resources/ObjectPageDoctor/DoctorMainMenuPage.robot
Resource   ../../../Resources/ObjectPageDoctor/RecommendPage.robot

# Test Setup = ก่อนที่จะเริ่มเข้า Test Case ให้เริ่มการ เปิด browser ขึ้นมาก่อน
Test Setup      CommonFunctionality.Start Test Case    
# Test Teardown = หลังจากจบ Test Case แต่ละ Case ให้ทำการ แคปรูปหน้าจอไว้ถ้ามีการ fail เกิดขึ้น
Test Teardown   Run Keyword If Test Failed    Capture Page Screenshot
# Suite Teardown = หลังจาก Run Test เสร็จทั้งหมดแล้วจะสั่งให้ทำ ปิด Browser ทั้งหมด
Suite Teardown  Close All Browsers

*** Variables ***
# Variables จะเป็นส่วนของการประกาศตัวแปรและค่าต่างๆ ที่จะใช้ในไฟล์ Robot นั้น
# sleeptime เป็นการกำหนดช่วงระยะเวลา ซึ่งจะนำไปใช้ในระหว่างคำสั่ง Script Test เพื่อป้องกันการเกิด Error ในกรณีที่เว็บไซต์ที่เทสโหลดหน้าเว็บไม่ทัน
${sleeptime}    1s

############## เป็นข้อมูล Username และ Password ที่ใช้ในการเข้าสู่ระบบ ##############################################################################################
# username password name_userdiagnose เป็นข้อมูลของ User แพทย์ที่ใช้ในการเข้าสู่ระบบ เพื่อวินิจฉัย
&{username}    valid=111111    invalid=100000
&{password}    valid=111111    invalid=111111
${name_userdiagnose}    ddddd ffff
# username password name_userdiagnose เป็นข้อมูลของ User เจ้าหน้าที่ที่ใช้ในการเข้าสู่ระบบ เพื่อในการออกรายการตรวจสุขภาพ
${username_fornewvn}    111111
${password_fornewvn}    111111
${name_fornewvn}    fff ddddd
############################################################################################################################################################

############## เป็นข้อมูล Config ใน Automate Script จะเป็นข้อมูลของ User ที่มีอยู่ในระบบอยู่แล้ว ห้ามแก้ ##################################################################
# profilepatient_data เป็นข้อมูล Profile ของผู้เข้ารับบริการที่ใช้ในการทดสอบ ไม่ต้องแก้
## เป็นข้อมูล Profile ของ Patient เป็นข้อมูลเดิมที่มีไม่ต้องแก้ 
&{profilepatient_data}    prefixth=นาย    prefixen=Mr.    nameth=ddddd    nameen=ffffff    lastnameth=dddddddd    lastnameen=llllll
...    idcard=111111    telno=11111111    birthdatefull=11111111    addressfull=11111111
...    hnno=PR46614160
# diagnosis_guide เป็น Config เลือกประเภทการวินิจฉัย ห้ามแก้
&{diagnosis_guide}    nextyear=อยู่ในเกณฑ์ปกติ    followup=ติดตามผลทุก 2 เดือน    conference=ติดตามผลและพบแพทย์ผ่านระบบทางไกล    meetdoctor=ส่งตัวพบแพทย์
# service_pressure เป็น Config เลือกรายการคัดกรอง ที่จะเลือกเฉพาะ คัดกรองเบื้องต้น (ความดัน)
&{service_pressure}    diabetes=0    pressure=1    sel_va=0    com_eye=0    pressure_eye=0    ccd=0    laser=0    oct=0    skin=0
...    heart=0    doctor=0
# config_firstgrader เป็น Config เพื่อบอกว่าเป็น Firstgrader ทำรายการ ห้ามแก้
&{config_firstgrader}    yes=1    no=0
############################################################################################################################################################

############## เป็นข้อมูลที่ใช้ในการ filter ค้นหารายการ / Insert บันทึกข้อมูลฟอร์ม ในหน้าจอเว็บไซต์นั้นๆ แก้ไขค่าได้ตามความต้องการ #############################################
# tabprimary_insert เป็นค่าที่จะ บันทึก รายการตรวจใหม่ที่จะให้หมอวินิจฉัย 
&{tabprimary_insert}    pressure_on=120    pressure_lower=90    blood_pressure=89    heartrate=80    spo2=98
...    typeblood_sugar=หลังทานอาหาร 2 ชม.    blood_sugar=150    temperature=36    weight=65    height=165    waistline=100
# diagnosis_pressure_1 เป็นการระบุคำวินิจฉัยความดัน เคส อยู่ในเกณฑ์ปกติ
&{diagnosis_pressure}    type=คำวินิจฉัยโรคแทรก    code=C100    additional=คิดว่าจะเป็นโรค ความดัน    templateupdate=พักผ่อนให้มากๆ(ความดัน) ทดสอบแก้ไขข้อความ
############################################################################################################################################################

*** Test Cases ***
Doctor : Diagnose pressure (Care guidelines : Normal)
    [Documentation]    (TC_DOCTOR_RECOMMEND_0013) Diagnose pressure (Care guidelines : Normal) 
    [Tags]             (TC_001) สร้าง VN + หมอวินิจฉัยโรคระบุ แนวทางการดูแล อยู่ในเกณฑ์ปกติ
    Comment    กำหนดระยะเวลาใน Script Automate ซึ่งจะใช้รอในระหว่างที่โหลดหน้าเว็บไม่ทัน
    CommonFunctionality.Set Time Out
    Comment    User เจ้าหน้าที่สร้างรายการตรวจสุขภาพใหม่ + เจ้าหน้าที่บันทึกผลตรวจสุขภาพ และเมื่อสรา้งรายการสำเร็จให้เก็บตัวเลขรายการตรวจ ไว้ที่ตัวแปร newvnno
    ${newvnno} =  RecommendPage.New VN and insert primary data    ${sleeptime}    ${username_fornewvn}    ${password_fornewvn}
    ...    ${name_fornewvn}    ${profilepatient_data.prefixth}    ${profilepatient_data.nameth}    ${profilepatient_data.lastnameth}
    ...    ${profilepatient_data.idcard}    ${service_pressure.diabetes}    ${service_pressure.pressure}    ${service_pressure.sel_va}
    ...    ${service_pressure.com_eye}    ${service_pressure.pressure_eye}    ${service_pressure.ccd}    ${service_pressure.laser}
    ...    ${service_pressure.oct}    ${service_pressure.skin}    ${service_pressure.heart}    ${service_pressure.doctor}
    ...    ${tabprimary_insert.pressure_on}    ${tabprimary_insert.pressure_lower}    ${tabprimary_insert.blood_pressure}
    ...    ${tabprimary_insert.heartrate}    ${tabprimary_insert.spo2}    ${tabprimary_insert.typeblood_sugar}
    ...    ${tabprimary_insert.blood_sugar}    ${tabprimary_insert.temperature}    ${tabprimary_insert.weight}
    ...    ${tabprimary_insert.height}    ${tabprimary_insert.waistline}
    Comment    User แพทย์ สร้าง Template คำแนะนำ + ค้นหาเลือกรายการที่จะวินิจฉัย
    RecommendPage.Doctor choose recommend record and Add Template    ${sleeptime}    ${username.valid}    ${password.valid}    ${name_userdiagnose}
    ...    ${newvnno}
    Comment    User แพทย์ เลือก tab ความดัน + ระบุข้อมูลคำวินิจฉัย + บันทึก
    RecommendPage.Add diagnosis (Tab pressure)    ${sleeptime}    ${diagnosis_pressure.type}    ${diagnosis_pressure.code}
    ...    ${diagnosis_pressure.additional}
    Comment    User แพทย์ ให้คำแนะนำโดยเลือกจาก Template ที่บันทึกมาก่อนหน้า + แก้ไขเพิ่มเติม 
    RecommendPage.Add sugguestion (Tab pressure) and Edit Text from Template    ${sleeptime}    ${diagnosis_pressure.templateupdate}
    Comment    User แพทย์ เลือกแนวทางการดูแล เป็น อยู่ในเกณฑ์ปกติ
    RecommendPage.Add guidelines by choose "Screening Next year" (Tab pressure)    ${sleeptime}
    Comment    User แพทย์ บันทึกผลการตรวจ และ ยืนยันบันทึกผลการตรวจ
    RecommendPage.Submit diagnose and Verify confirm popup (Tab pressure)    ${sleeptime}    ${diagnosis_pressure.type}
    ...    ${diagnosis_pressure.code}    ${diagnosis_pressure.additional}    ${diagnosis_pressure.templateupdate}
    ...    ${diagnosis_guide.nextyear}    ${config_firstgrader.no}   
    Comment    User แพทย์ ค้นหารายการตรวจอีกครั้ง ซึ่งรายการที่เพิ่งวินิจฉัยแล้วจะต้องหายไป และทำการลบ Template คำแนะนำ ที่สร้างไป
    RecommendPage.Search vn record after diagnose (pressure) and Delete template   ${sleeptime}    ${diagnosis_pressure.type}    ${diagnosis_pressure.code}
    ...    ${diagnosis_pressure.additional}    ${diagnosis_pressure.templateupdate}    ${username_fornewvn}    ${password_fornewvn}
    ...    ${name_fornewvn}    ${name_userdiagnose}    ${diagnosis_guide.nextyear}    ${config_firstgrader.no}
    Comment    ปิด Browsers ทั้งหมด
    Close All Browsers
