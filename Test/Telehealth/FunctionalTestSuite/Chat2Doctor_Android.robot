*** Settings ***
# Settings ส่วนของการตั้งค่าให้กับไฟล์ Robot ของเรา ไม่ว่าจะเป็นการ Import Library, Resource หรือแม้กระทั่ง Setup คำสั่งต่างๆ ก่อนเริ่มทดสอบ
Library     AppiumLibrary

Documentation    Menu "แอปคุยกับหมอ ประชาชนและเจ้าหน้าที่"
Resource   ../../../Resources/ObjectChat2Doctor/User.robot
Resource   ../../../Resources/ObjectChat2Doctor/Officer.robot

# Suite Teardown = หลังจาก Run Test เสร็จทั้งหมดแล้วจะสั่งให้ทำ ปิด Browser ทั้งหมด
Test Teardown   Run Keyword If Test Failed    Capture Page Screenshot

*** Variables ***
# Variables จะเป็นส่วนของการประกาศตัวแปรและค่าต่างๆ ที่จะใช้ในไฟล์ Robot นั้น
# sleeptime เป็นการกำหนดช่วงระยะเวลา ซึ่งจะนำไปใช้ในระหว่างคำสั่ง Script Test เพื่อป้องกันการเกิด Error ในกรณีที่เว็บไซต์ที่เทสโหลดหน้าเว็บไม่ทัน
${sleeptime}    1s

# Clinic_username Clinic_password Clinic_name Clinic_surname เป็นข้อมูลของ User ปชช ประเภทคลีนิคออนไลน์
&{Clinic_username}    valid=1111111    invalid=100000
&{Clinic_password}    valid=1111111    invalid=111111
${Clinic_name}    สมบัติ
${Clinic_surname}    ดีพร้อม

# TeleHealth_name TeleHealth_surname TeleHealth_idcard TeleHealth_tel TeleHealth_Username เป็นข้อมูลของ User ปชช ประเภทโครงการเทเลเฮลท์
${TeleHealth_name}    ffff
${TeleHealth_surname}    sssss
${TeleHealth_idcard}    1111
${TeleHealth_tel}    1111111111
${TeleHealth_Username}    1111111

# Officer_username Officer_password Officer_fullname Officer_hospitalname เป็นข้อมูลของ จนท
&{Officer_username}    valid=111111    invalid=100000
&{Officer_password}    valid=111111    invalid=111111
${Officer_fullname}    ffffff ssssssss
${Officer_hospitalname}    รพ.สต.ทดสอบ ต.ทดสอบ อ.ทดสอบ จ.ทดสอบ

*** Test Cases ***
Login User Clinic Online (First time)
    [Documentation]    (TC_MOBILE_001) Login User Clinic Online (First time)
    [Tags]             (TC_MOBILE_001) ปชชล็อคอินแอปคุยกับหมอ โดยใช้ User ประเภทคลีนิคออนไลน์ (6 เมนู คุยกับหมอ , สมุดสุขภาพ , คัดกรองผ่านระบบเทเลเฮลท์ , คลังความรู้ , สมุดสุขภาพ และตั้งค่า)
    Comment    เปิด Application คุยกับหมอ
    User.Open Android Application User (First time)
    Comment    ตรวจสอบ หน้าจอหลัก
    User.Verify Main Page    ${sleeptime}
    Comment    ตรวจสอบ หน้า form ล็อคอิน
    User.Verify Login menu    ${sleeptime}
    Comment    เข้าสู่ระบบ ด้วย  User ปชช ประเภทคลีนิคออนไลน์
    User.Login clinic Online user    ${sleeptime}    ${Clinic_username.valid}    ${Clinic_password.valid}
    Comment    ยอมรับ เงื่อนไขการใช้บริการ
    User.Accept term and condition    ${sleeptime}
    Comment    ตรวจสอบหน้าจอ ตั้งค่าความปลอดภัย PIN
    User.Verify skip pin menu    ${sleeptime}
    Comment    ตั้งค่า PIN 4 หลัก
    User.Setting PIN    ${sleeptime}
    Comment    ตรวจสอบ 6 เมนูหลัก
    User.Check Main Menu App    ${sleeptime}    ${Clinic_name}    ${Clinic_surname}
    Comment    ตรวจสอบหน้าจอ เมนู "คุยกับหมอ"
    User.Check "Chat2Doctor" Menu    ${sleeptime}
    Comment    ตรวจสอบหน้าจอ เมนู "สมุดสุขภาพ"
    User.Check "Health Book" Menu    ${sleeptime}    ${Clinic_name}    ${Clinic_surname}
    Comment    ตรวจสอบหน้าจอ เมนู "คัดกรองผ่านระบบเทเลเฮลท์"
    User.Check "Telehealth" Menu   ${sleeptime}
    Comment    ตรวจสอบหน้าจอ เมนู "คลังความรู้"
    User.Check "Knowledge Management" Menu    ${sleeptime}
    Comment    ตรวจสอบหน้าจอ เมนู "กล่องข้อความ"
    User.Check "Message Box" Menu    ${sleeptime}
    Comment    ตรวจสอบหน้าจอ เมนู "ตั้งค่า"
    User.Check "Setting" Menu    ${sleeptime}    ${Clinic_name}    ${Clinic_surname}    ${Clinic_username.valid}

Login User TeleHealth (First time)
    [Documentation]    (TC_MOBILE_002) Login User TeleHealth (First time)
    [Tags]             (TC_MOBILE_002) ปชชล็อคอินแอปคุยกับหมอ โดยใช้ User ประเภทโครงการเทเลเฮลท์ (6 เมนู คุยกับหมอ , สมุดสุขภาพ , คัดกรองผ่านระบบเทเลเฮลท์ , คลังความรู้ , สมุดสุขภาพ และตั้งค่า)
    Comment    ปิด Application ทั้งหมด
    Close All Applications
    Comment    เปิด Application คุยกับหมอ
    User.Open Android Application User (First time)
    Comment    ตรวจสอบ หน้าจอหลัก
    User.Verify Main Page    ${sleeptime}
    Comment    ตรวจสอบ หน้า form ล็อคอิน
    User.Verify Login menu    ${sleeptime}
    Comment    เข้าสู่ระบบ ด้วย  User ปชช ประเภทโครงการเทเลเฮลท์
    User.Login telehelth user    ${sleeptime}    ${TeleHealth_idcard}    ${TeleHealth_tel}
    Comment    ตรวจสอบหน้าจอ ยืนยันรหัส OTP
    User.Vefify OTP    ${sleeptime}
    Comment    ยอมรับ เงื่อนไขการใช้บริการ
    User.Accept term and condition    ${sleeptime}
    Comment    ตรวจสอบหน้าจอ ตั้งค่าความปลอดภัย PIN
    User.Verify skip pin menu    ${sleeptime}
    Comment    ตั้งค่า PIN 4 หลัก
    User.Setting PIN    ${sleeptime}
    Comment    ตรวจสอบ 6 เมนูหลัก
    User.Check Main Menu App    ${sleeptime}    ${TeleHealth_name}    ${TeleHealth_surname}
    Comment    ตรวจสอบหน้าจอ เมนู "คุยกับหมอ"
    User.Check "Chat2Doctor" Menu (User TeleHealth)    ${sleeptime}
    Comment    ตรวจสอบหน้าจอ เมนู "สมุดสุขภาพ"
    User.Check "Health Book" Menu (User TeleHealth)    ${sleeptime}    ${TeleHealth_name}    ${TeleHealth_surname}
    Comment    ตรวจสอบหน้าจอ เมนู "คัดกรองผ่านระบบเทเลเฮลท์"
    User.Check "Telehealth" Menu (User TeleHealth)    ${sleeptime}
    Comment    ตรวจสอบหน้าจอ เมนู "คลังความรู้"
    User.Check "Knowledge Management" Menu (User TeleHealth)    ${sleeptime}
    Comment    ตรวจสอบหน้าจอ เมนู "กล่องข้อความ"
    User.Check "Message Box" Menu (User TeleHealth)    ${sleeptime}
    Comment    ตรวจสอบหน้าจอ เมนู "ตั้งค่า"
    User.Check "Setting" Menu (User TeleHealth)    ${sleeptime}    ${TeleHealth_name}    ${TeleHealth_surname}    ${TeleHealth_Username} 

Re-Login User
    [Documentation]    (TC_MOBILE_003) Re-Login User
    [Tags]             (TC_MOBILE_003) ปชชล็อคอินแอปคุยกับหมอ (เข้าแอปที่ล็อคอินไว้แล้ว)
    Comment    ปิด Application ทั้งหมด
    Close All Applications
    Comment    เปิด Application คุยกับหมอ ที่พับหน้าจอไว้
    User.Open Android Application User (Already Login)
    Comment    ระบุ PIN 4 หลัก
    User.Verify PIN    ${sleeptime}
    Comment    ตรวจสอบ 6 เมนูหลัก
    User.Check Main Menu App    ${sleeptime}    ${TeleHealth_name}    ${TeleHealth_surname}
    Comment    ออกจากระบบ
    User.User logout    ${sleeptime}

Login Officer (First time)
    [Documentation]    (TC_MOBILE_004) Login Officer (First time)
    [Tags]             (TC_MOBILE_004) เจ้าหน้าที่ล็อคอินแอปคุยกับหมอ (6 เมนู ผู้เข้ารับบริการวันนี้ , รายการนัดหมายจากแพทย์ , ประวัติผู้เข้ารับบริการ , ระบบประชุมทางไกล , นัดหมายขอคำปรึกษา และระบบปรึกษาปัญหาสุขภาพ)
    Comment    ปิด Application ทั้งหมด
    Close All Applications
    Comment    เปิด Application คุยกับหมอ (สำหรับเจ้าหน้าที่)
    Officer.Open Android Application Officer (First time)
    Comment    เข้าสู่ระบบ ด้วย  User เจ้าหน้าที่
    Officer.Login Officer user    ${sleeptime}    ${Officer_username.valid}    ${Officer_password.valid}
    Comment    ตรวจสอบ 6 เมนูหลัก
    Officer.Check Officer Main Menu    ${sleeptime}    ${Officer_fullname}    ${Officer_hospitalname}
    Comment    ตรวจสอบหน้าจอ เมนู "ผู้เข้ารับบริการวันนี้"
    Officer.Check "List Daily" Menu    ${sleeptime}    ${Officer_fullname}
    Comment    ตรวจสอบหน้าจอ เมนู "รายการนัดหมายจากแพทย์"
    Officer.Check "List Appointment" Menu     ${sleeptime}    ${Officer_fullname}
    Comment    ตรวจสอบหน้าจอ เมนู "ประวัติผู้เข้ารับบริการ"
    Officer.Check "List History" Menu    ${sleeptime}    ${Officer_fullname}
    Comment    ตรวจสอบหน้าจอ เมนู "ระบบประชุมทางไกล"
    Officer.Check "Conference" Menu    ${sleeptime}    ${Officer_fullname}
    Comment    ตรวจสอบหน้าจอ เมนู "นัดหมายขอคำปรึกษา"
    Officer.Check "Appointment" Menu    ${sleeptime}    ${Officer_fullname}
    Comment    ตรวจสอบหน้าจอ เมนู "ระบบปรึกษาปัญหาสุขภาพ"
    Officer.Check "Private Chat" Menu   ${sleeptime}

Re-Login User Officer
    [Documentation]    (TC_MOBILE_005) Re-Login User Officer
    [Tags]             (TC_MOBILE_005) เจ้าหน้าที่ล็อคอินแอปคุยกับหมอ (เข้าแอปที่ล็อคอินไว้แล้ว)
    Comment    ปิด Application ทั้งหมด
    Close All Applications
    Comment    เปิด Application คุยกับหมอ (สำหรับเจ้าหน้าที่) ที่พับหน้าจอไว้
    Officer.Open Android Application Officer (Already Login)
    Comment    ตรวจสอบ 6 เมนูหลัก
    Officer.Check Officer Main Menu    ${sleeptime}    ${Officer_fullname}    ${Officer_hospitalname}
    Comment    ออกจากระบบ
    Officer.Officer logout    ${sleeptime}
    Comment    ปิด Application ทั้งหมด
    Close All Applications